<?php
/*
Plugin Name: WP Memba
Plugin URI: http://www.thesiteweb.com
Description: Members management plugin
Author: CAISSON Frederic
Version: 1.0
Author URI: http://www.thesiteweb.com
*/
define('THIS_PLUGIN_NAME', 'memba');
define('THIS_PLUGIN_BASE_DIR', dirname(__FILE__).'/');
define('THIS_PLUGIN_BASE_DIR_HTTP',plugins_url( '', __FILE__ ));

if(defined('STDIN') )
{
    //command line
}
else
{
    if (version_compare(phpversion(), '5.4.0', '>=')) {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
    }
    else {
        if(!session_id())
            session_start();
    }
}
require_once(THIS_PLUGIN_BASE_DIR.'app/base/Util.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/base/Model.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/base/View.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/base/Controller.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/model/Currency.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/model/User.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/model/UserProduct.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/model/Page.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/model/ProductContent.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/model/Product.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/model/PaypalPeriod.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/model/Config.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/model/InvoiceLog.php');

require_once(THIS_PLUGIN_BASE_DIR.'app/controller/AdminController.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/controller/AdminMemberController.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/controller/AdminPageController.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/controller/AdminProductController.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/controller/AdminPaymentController.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/controller/PublicController.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/controller/PublicDashboardController.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/controller/PublicLoginController.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/controller/PublicMemberController.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/controller/PublicProductController.php');
//require_once(THIS_PLUGIN_BASE_DIR.'app/controller/PublicIpnController.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/controller/PublicPaypalController.php');
require_once(THIS_PLUGIN_BASE_DIR.'app/base/Session.php');
//require_once(THIS_PLUGIN_BASE_DIR.'lib/paypal/autoload.php');

add_action( 'plugins_loaded', array( 'PublicController', 'load' ) );

if (is_admin()) {
   add_action( 'plugins_loaded', array( 'AdminController', 'load' ) );
}

?>
