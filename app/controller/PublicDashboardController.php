<?php
class PublicDashboardController extends Controller
{
	public function dashboard(){
		$pageModel = new Page();

		$response = $pageModel->checkPageSettings();
		if($response['result']){
			if(isset($_GET['view'])){
				$view = $_GET['view'];
			} else {
				$view = 'user_dashboard';
			}

			$user = Session::getConnectedUser();
			$dashboardPageId = $pageModel->findBySlug(Page::PAGE_DASHBOARD_SHORTCODE);
			$dashboardPermalink = get_permalink( $dashboardPageId );

			$includeViews[] = 'user_dashboard_menu';
			$params = array('user_dashboard_page' => $dashboardPermalink,
							'current_view' => $view
			);

			switch($view) {
				case 'user_dashboard_products' :
					$productContentModel = new ProductContent();
					$products = $productContentModel->findUserProductAndContent($user);
					$params['products'] = $products;
				break;
				case 'user_dashboard_settings' :
					wp_enqueue_script('settings-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/settings.js',array( 'jquery' ));
					wp_localize_script( 'settings-script', 'ajax_object', 
						array( 'ajax_url' => admin_url( 'admin-ajax.php' ))
					);
					$params['user'] = $user;
				break;
				case 'user_logout' :
					$loginPageId = $pageModel->findBySlug(Page::PAGE_LOGIN_SHORTCODE);
					$loginPermalink = get_permalink( $loginPageId );
					$params['user_login_page'] = $loginPermalink;
					Session::resetSession();
				break;
			}

			$this->renderView($view,$params,$includeViews);

		} 
	}
}
?>