<?php
class AdminMemberController extends Controller
{
	function listMembers(){

		wp_enqueue_script('admin-listmembers-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/admin-listmembers.js',array( 'jquery' ));
        wp_localize_script( 'admin-listmembers-script', 'ajax_object', 
                    array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 
                    	   'confirm_message' => __('Are you sure you want to delete this record?'))
                    );

		$menu_members = $this->getData('menu_members');
		$itemsPerPage = 50;
    	$page = 1;

    	$userModel = new User();
    	list($rows, $pagination) = $userModel->findAll($itemsPerPage, $page, $menu_members['slug']);

    	$includeViews[] = 'admin_menu';
    	$view = 'admin_members';

    	$params = array('rows' => $rows,
    				    'pagination' => $pagination,
    				    'menu_member_slug' => $menu_members['slug'],
    	);

    	$this->renderView($view,$params,$includeViews);
	}

	function createMember(){
		$user = new StdClass();
		$menu_members = $this->getData('menu_members');
		
		if (isset($_POST['submit'])) {

			$user->email = $_POST['email'];
			$user->first_name = $_POST['first_name'];
			$user->last_name = $_POST['last_name'];
			$user->tel = $_POST['tel'];
			$user->postal = $_POST['postal'];
			$user->address1 = $_POST['address1'];
			$user->address2 = $_POST['address2'];
			$user->password = $_POST['password'];
			$user->timezone = $_POST['timezone'];
			$userModel = new User();
			$response = $userModel->valid($user);

			if($response['result'] == 1) {
				$userModel->save($user);
				$response['message'] = __('Saved', THIS_THEME_NAME);
			} 
		}


		$includeViews[] = 'admin_menu';
		$view = 'admin_member_create';
		$params = array('menu_member_slug' => $menu_members['slug'],
						'user' => $user,
						'response' => $response,
						'timezones' => Util::getTimezones());
		$this->renderView($view,$params,$includeViews);
	}

	function editMember($userId){

		$userModel = new User();
		$user = $userModel->findById($userId);
		$response = null;
		$menu_members = $this->getData('menu_members');
		if (isset($_POST['submit'])) {

			$user->email = $_POST['email'];
			$user->first_name = $_POST['first_name'];
			$user->last_name = $_POST['last_name'];
			$user->tel = $_POST['tel'];
			$user->postal = $_POST['postal'];
			$user->address1 = $_POST['address1'];
			$user->address2 = $_POST['address2'];
			$user->password = $_POST['password'];
			$user->timezone = $_POST['timezone'];
			$response = $userModel->valid($user);

			if($response['result'] == 1) {
				$userModel->save($user);
				$user = $userModel->findById($userId);
				$response['message'] = __('Saved', THIS_THEME_NAME);
			} 
		}


		$includeViews[] = 'admin_menu';
		$view = 'admin_member_edit';
		$params = array('menu_member_slug' => $menu_members['slug'],
						'user' => $user,
						'response' => $response,
						'timezones' => Util::getTimezones());
		$this->renderView($view,$params,$includeViews);
	}

	function deleteMember(){

		if (isset($_POST['id'])) {

			$userModel = new User();
			$user = $userModel->findById($_POST['id']);

			$userProductModel = new UserProduct();
			$products = $userProductModel->findUserProducts($user);

			if (count($products) == 0) {

				$userModel->delete($user);
				$response = array('result' => 1, 'message'=> __('Deleted',THIS_PLUGIN_NAME));
			} else {
				$response = array('result' => 0, 'message'=> __('This user bought some products. Deletion is not possible',THIS_PLUGIN_NAME));
			}
			
			echo json_encode($response);
		}
		die();
	}
}

?>