<?php

class PublicIpnController extends Controller
{
	public function log($status, $log, $post){
		$invoiceLog = new StdClass();
    	$invoiceLog->invoice_id = $post['invoice'];
    	$invoiceLog->txn_type = $post['txn_type'];
    	$invoiceLog->ipn_track_id = $post['ipn_track_id'];
    	$invoiceLog->txn_id = $post['txn_id'];
    	$invoiceLog->payer_id = $post['payer_id'];
    	$invoiceLog->payer_email = $post['payer_email'];
    	$invoiceLog->status = $status;
    	$invoiceLog->log = $log;
    	$invoiceLog->data = json_encode($post);
    	$invoiceLogModel = new InvoiceLog();
    	$invoiceLogModel->log($invoiceLog);
    	unset($invoiceLog);
    	unset($invoiceLogModel);
	}

	public function notify(){
		if(!isset($_POST['txn_type']) || !isset($_POST['invoice'])){
			echo __('Bad request');
			exit();
		}
		$this->log(InvoiceLog::STATUS_INFO, __('Info', THIS_PLUGIN_NAME), $_POST);
	}

	public function thank(){
		
	}
}


?>