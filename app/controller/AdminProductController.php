<?php
class AdminProductController extends Controller
{
	public function listProducts(){
		wp_enqueue_script('admin-listproducts-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/admin-listproducts.js',array( 'jquery' ));
        wp_localize_script( 'admin-listproducts-script', 'ajax_object', 
                    array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 
                    	  'confirm_message' => __('Are you sure you want to delete this product?'))
                    );
        $itemsPerPage = 50;
    	$page = 1;
        $productModel = new Product();
        $menu_products = $this->getData('menu_products');

        list($rows, $pagination) = $productModel->findAll($itemsPerPage, $page, $menu_products['slug']);

        $includeViews[] = 'admin_menu';
    	$view = 'admin_products';
    	
    	$params = array('rows' => $rows,
    				    'pagination' => $pagination,
    				    'menu_product_slug' => $menu_products['slug'],
    	);


    	$this->renderView($view,$params,$includeViews);
	}


    function createProduct(){
        wp_enqueue_script('admin-product-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/admin-product.js',array( 'jquery' ));
        $product = new StdClass();
        $menu_products = $this->getData('menu_products');
        $productModel = new Product();

        if (isset($_POST['submit'])) {

            $product->billing_interval = Model::NULL;
            $product->billing_period = Model::NULL;
            $product->billing_cycles = Model::NULL;
            $product->trial_period = Model::NULL;
            $product->trial_interval = Model::NULL;
            $product->trial_price = Model::NULL;
            $product->bought_price = Model::NULL;
            $product->stock = Model::NULL;
            $product->tax = Model::NULL;
            $product->shipping_fees = Model::NULL;

            $product->name = $_POST["name"];
            $product->currency = $_POST["currency"];
            $product->type = intval($_POST['type']);

            if ($product->type == 4 || $product->type == 6) {
                
                list($product->trial_period, $product->trial_interval) = explode(":",$_POST["trial_period"]);
                $product->trial_interval = intval($product->trial_interval);
                $product->trial_price = $_POST["trial_price"];
            }
            
            if ($product->type > 1) {
                list($product->billing_period, $product->billing_interval) = explode(":",$_POST["sell_period"]);
                $product->billing_interval = intval($product->billing_interval);
            } 

            if ($product->type > 4) {
                $product->billing_cycles = intval($_POST["billing_cycles"]);
            }

            $product->sell_price = floatval($_POST["sell_price"]);
            

            if(!empty($_POST["bought_price"])){
                $product->bought_price = $_POST["bought_price"];
            }
            if(!empty($_POST["stock"])){
                $product->stock = $_POST["stock"];
            }
            if(!empty($_POST["tax"])){
                $product->tax = $_POST["tax"];
            }
            if(!empty($_POST["shipping_fees"])){
                $product->shipping_fees = $_POST["shipping_fees"];
            }

            $response = $productModel->valid($product);

            if($response['result'] == 1) {
                $productModel->save($product);
                $response['message'] = __('Saved', THIS_THEME_NAME);
            } 
        }
        $currencyModel = new Currency();
        $currencies = $currencyModel->findAll();
        
        $paypalPeriodModel = new PaypalPeriod();
        $paypalPeriods = $paypalPeriodModel->findAll();
        $types = Product::$types;
        $includeViews[] = 'admin_menu';
        $view = 'admin_product_create';
        $params = array('menu_product_slug' => $menu_products['slug'],
                        'product' => $product,
                        'types' => $types,
                        'response' => $response,
                        'currencies' => $currencies,
                        'paypal_periods' => $paypalPeriods);
        $this->renderView($view,$params,$includeViews);
    }
}
?>