<?php
class AdminPageController extends Controller
{
	public function listPages(){
		wp_enqueue_script('admin-listpages-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/admin-listpages.js',array( 'jquery' ));
        wp_localize_script( 'admin-listpages-script', 'ajax_object', 
                    array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 
                    	  'confirm_message' => __('Are you sure you want to delete this page?'))
                    );

		$pageModel = new Page();
		
        $labels = array(
					Page::PAGE_LOGIN_SHORTCODE    =>  __("Login page", THIS_PLUGIN_NAME),
					Page::PAGE_DASHBOARD_SHORTCODE =>  __("User's dashboard", THIS_PLUGIN_NAME),
				);
		$items = array(
					Page::PAGE_LOGIN_SHORTCODE    =>  $_POST[Page::PAGE_LOGIN_SHORTCODE],
					Page::PAGE_DASHBOARD_SHORTCODE =>  $_POST[Page::PAGE_DASHBOARD_SHORTCODE],
				);

		$response = $pageModel->valid($items);


		if($_POST['delete']) {

			if($_POST[Page::PAGE_LOGIN_SHORTCODE] == $_POST[Page::PAGE_DASHBOARD_SHORTCODE]){
				$response = array('result'=> 0, message => __('You cannot assign the same page to login and dashboard', THIS_PLUGIN_NAME));
				//$view->set('message', $message);
			} else {
				
				
				
				$pageModel->saveAll($items);
				$response = array('result'=> 1, message => __('Saved', THIS_PLUGIN_NAME));
				//$view->set('message', $message);
			}
		} else {

			$items = array(
				Page::PAGE_LOGIN_SHORTCODE     =>  $pageModel->findBySlug(Page::PAGE_LOGIN_SHORTCODE),
				Page::PAGE_DASHBOARD_SHORTCODE =>  $pageModel->findBySlug(Page::PAGE_DASHBOARD_SHORTCODE),
			);
		}

		$params = array('select_page_params' => array('show_option_none' => '- '.__('Select a page',THIS_PLUGIN_NAME).' -'),
						'response' => $response,
						'items' => $items,
						'labels' => $labels,
						'pageSettings' => $pageModel->checkPageSettings()
			      		);


		$includeViews[] = 'admin_menu';
		$view = 'admin_pages';
		$this->renderView($view,$params,$includeViews);
	}


	public function createPage(){
		$pageModel = new Page();

		if(!$pageModel->findbySlug($_POST['slug'])){

			if(isset($_POST['slug']) && isset($_POST['page_name'])){
				$page = array(
					'post_title' => $_POST['page_name'],
					'post_content' => "[{$_POST['slug']}]",
					'post_status' => 'publish',
					'post_type' => 'page',
				);

				$postId = wp_insert_post($page);

				
				$pageModel->save($_POST['slug'],$postId);

				$response = array('result'=> 1, message => __('Saved', THIS_PLUGIN_NAME));
			}

		} else {
			$response = array('result'=> 0, message => __('Error', THIS_PLUGIN_NAME));
		}
		echo json_encode($response);
		die();
	}

	public function deletePage(){

		if (isset($_POST['page_id'])) {

			wp_delete_post( $_POST['page_id'], true );

			$pageModel = new Page();
			$pageModel->delete($_POST['page_id']);
			$response = array('result' => 1, 'message'=> __('Deleted',THIS_PLUGIN_NAME));
			
			echo json_encode($response);
		}
		die();
	}
}

?>