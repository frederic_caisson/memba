<?php
class PublicLoginController extends Controller
{
	public function login(){
		$pageModel = new Page();
		$response = $pageModel->checkPageSettings();
		if($response['result']){
			wp_enqueue_script('login-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/login.js',array( 'jquery' ));

			$loginPageId = $pageModel->findBySlug(Page::PAGE_LOGIN_SHORTCODE);
			$dashboardPageId = $pageModel->findBySlug(Page::PAGE_DASHBOARD_SHORTCODE);
			
			$dashboardPermalink = get_permalink( $dashboardPageId );

			wp_localize_script( 'login-script', 'ajax_object', 
				array( 'ajax_url' => admin_url( 'admin-ajax.php' ),
					   'redirect_url' => $dashboardPermalink,
				)
			);

			$pageId = get_the_ID();
			if(intval($pageId) == intval($loginPageId)){
				$params = array ('user_dashboard_page' => $dashboardPermalink);
				$this->renderView('user_login',$params);
			} else {
				echo __('You must generate this page via your admin interface, please delete this page',THIS_PLUGIN_NAME);
			}
		}
		
	}


	public function checkLogin(){

		$response = array('result' => 0, 'message'=> __('Invalid',THIS_PLUGIN_NAME));

		if(isset($_POST['email']) && isset($_POST['password'])) {
			$email = $_POST['email'];
			$password = $_POST['password'];
			$userModel = new User();
			$user = $userModel->findByEmail($email);
	        if ($user) {
	        	if($user->password == $password) {
					Session::createSession($user->id);
					$response = array('result' => 1, 'message'=> __('Success',THIS_PLUGIN_NAME));
				} else {
					$response = array('result' => 0, 'message'=> __('Entered password is invalid',THIS_PLUGIN_NAME));
				}
			} else {
				$response = array('result' => 0, 'message'=> __('This email is not registred',THIS_PLUGIN_NAME));
			}
    	}
    	echo json_encode($response);
		die();
	}
}
?>