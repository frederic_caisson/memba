<?php
class PublicController extends Controller
{
	public function __construct() {
		parent::__construct();
		$this->publicLoginController = new PublicLoginController();
		$this->publicDashboardController = new PublicDashboardController();
		$this->publicMemberController = new PublicMemberController();
		$this->publicProductController = new PublicProductController();
		$this->publicPaypalController = new PublicPaypalController();
		
		add_action('template_redirect', array($this, 'checkAccess'));

		add_action( 'wp_ajax_check_login', array( $this->publicLoginController, 'checkLogin' ) );
		add_action( 'wp_ajax_nopriv_check_login', array( $this->publicLoginController, 'checkLogin' ) );

		add_action( 'wp_ajax_save_member', array( $this->publicMemberController, 'saveMember' ) );
		add_action( 'wp_ajax_nopriv_save_member', array( $this->publicMemberController, 'saveMember' ) );

		add_action( 'wp_ajax_process_checkout', array( $this->publicPaypalController, 'process' ) );
		add_action( 'wp_ajax_process_checkout', array( $this->publicPaypalController, 'process' ) );


		//Shortcodes
		add_shortcode( Page::PAGE_LOGIN_SHORTCODE, array( $this->publicLoginController, 'login' ) );
		add_shortcode( Page::PAGE_DASHBOARD_SHORTCODE, array( $this->publicDashboardController, 'dashboard' ) );
		add_shortcode( Product::PRODUCT_BUTTON_SHORTCODE, array( $this->publicProductController , 'getProductButton' ) );

		wp_enqueue_style( 'awsome-font-style', '//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' );
	
		wp_enqueue_style( THIS_PLUGIN_NAME.'-style', THIS_PLUGIN_BASE_DIR_HTTP.'/css/style.css' );

		wp_enqueue_script('common-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/common.js');
	}

	public function checkAccess(){
	
		if($_GET[THIS_PLUGIN_NAME.'_thank']){
			$this->publicPaypalController->thank();
			exit();
		} else if($_GET[THIS_PLUGIN_NAME.'_cancel']) {
			$this->publicPaypalController->cancel();
			exit();
		} else if($_GET[THIS_PLUGIN_NAME.'_process']) {
			$this->publicPaypalController->process();
			exit();
		} else if($_GET[THIS_PLUGIN_NAME.'_check']) {
			$this->publicPaypalController->checkout();
			exit();
		} else {
	   
			if(is_page()) {
				$pageModel = new Page();
				$loginPageId = $pageModel->findBySlug(Page::PAGE_LOGIN_SHORTCODE);
				$loginPermalink = get_permalink( $loginPageId );
				$dashboardPageId = $pageModel->findBySlug(Page::PAGE_DASHBOARD_SHORTCODE);
				$currentPageId = strval(get_the_ID());
				$user = Session::getConnectedUser();
			
				if($currentPageId === $dashboardPageId) {
					
					if(!$user) {
						wp_redirect($loginPermalink);
			    		exit();
					} 

				} else {

					$productContentModel = new ProductContent();
					$privatePagesIds = $productContentModel->findAllPageIds();

					if(in_array($currentPageId,$privatePagesIds)) {

					    if(!$user) {

					    	wp_redirect($loginPermalink);
					    	exit();

					    } else {

					    	$accessiblePageIds = $productContentModel->findUserAccessiblePageIds($user);
					    	if(!in_array($currentPageId,$accessiblePageIds)) {
					    		wp_redirect($loginPermalink);
					    		exit();
					    	}
					    }
					}
				}
			}
		}
	}
}
?>