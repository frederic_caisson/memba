<?php

class PublicProductController extends Controller
{

	function getProductButton($atts){
		wp_enqueue_script('settings-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/product.js',array( 'jquery' ));
		$param = THIS_PLUGIN_NAME.'_check';
		$url = site_url().'/?'.$param.'=1';

		$productModel = new Product();
		$product = $productModel->findById($atts['id']);
		$labelButton = __('Buy');

		$str .= "<form action='$url' method='post'>";
		$str .= "<input type='hidden' name='product_id' value='{$product->id}'/>";
		$str .= "<input type='submit' value='$labelButton'>";
		$str .= "</form>";

		return $str;
	}

	function getProductButton2($atts)
	{
		wp_enqueue_script('settings-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/product.js',array( 'jquery' ));
		//https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables/#id08A6HI00JQU
		$email = '';
		$url = '';
		$ipn_url = site_url();
		$notify_param = THIS_PLUGIN_NAME.'_notify';
		$thank_param = THIS_PLUGIN_NAME.'_thank';

		$productModel = new Product();
		$product = $productModel->findById($atts['id']);
		$configModel = new Config();
		$config = $configModel->findByKey(Config::PAYMENT_PAYPAL_KEY);

		if ($config) {
			if($config->mode == 0) {
				$url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
				$email = $config->paypal_email_sandbox;
			} else {
				$url = 'https://www.paypal.com/cgi-bin/webscr';
				$email = $config->paypal_email;
			}
		}

		var_dump($product);
		$str = '';
		if($product){

			$invoiceId = time().'-'.mt_rand().'-'.$product->id;

			if($product->type > 1){
				$type = '_xclick-subscriptions';
			} else {
				$type = '_xclick';
			}
			$str .= '<form action="'.$url.'" method="post">';
	        $str .= '<input type="hidden" name="business" value="'.$email.'" />';
	        $str .= '<input type="hidden" name="cmd" value="'.$type.'" />';
	        $str .= '<input type="hidden" name="notify_url" value="'.$ipn_url.'/?'.$notify_param.'=1" />';
	        $str .= '<input type="hidden" name="cancel_return" value="'.$ipn_url.'" />';
	        $str .= '<input type="hidden" name="return" value="'.$ipn_url.'/?'.$thank_param.'=1" />';
	        $str .= '<input type="hidden" name="rm" value="2" />';
	        
	        $str .= '<input type="hidden" name="lc" value="" />';

	        $str .= '<input type="hidden" name="no_shipping" value="1" />';
	        $str .= '<input type="hidden" name="no_note" value="1" />';
	        $custom = "product_id={$product->id}";
	        $str .= '<input type="text" class="custom-field" name="custom" value="'.$custom.'" />';
	        $str .= '<input type="hidden" name="currency_code" value="'.$product->currency.'" />';
	        $str .= '<input type="hidden" name="page_style" value="paypal" />';
	        $str .= '<input type="hidden" name="charset" value="utf-8" />';
	        $str .= '<input type="hidden" name="item_name" value="'.$product->name.'" />';

	        $str .= '<input type="hidden" name="invoice" value="'.$invoiceId.'" />';

	        $str .= '<input type="hidden" name="modify" value="0" />';

	        
	        if ($product->type > 3) {
	        	$recurring = 1;
	        } else {
	        	$recurring = 0;
	        }
	        $str .= '<input type="hidden" name="src" value="'.$recurring.'" />';
	        $str .= '<input type="hidden" name="srt" value="" />';
	        $str .= '<input type="hidden" name="sra" value="1" />';

	        //trial
	        if ($product->type == 3 || $product->type == 5) {
		        $str .= '<input type="hidden" name="a1" value="'.$product->trial_price.'" />';
		        $str .= '<input type="hidden" name="p1" value="'.$product->trial_interval.'" />';
		        $str .= '<input type="hidden" name="t1" value="'.$product->trial_period.'" />';
	    	}

	    	//sell
	    	if ($product->type == 1){
	    		$str .= '<input type="hidden" name="amount" value="'.$product->sell_price.'" />';
	    		//TODO : discount
	    		$str .= '<input type="hidden" name="discount_amount" value="0" />';
	        } else {
		        $str .= '<input type="hidden" name="a3" value="'.$product->sell_price.'" />';
		        $str .= '<input type="hidden" name="p3" value="'.$product->billing_interval.'" />';
		        $str .= '<input type="hidden" name="t3" value="'.$product->billing_period.'" />';
	    	}

	        $str .= '<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" style="width:auto; height:auto; border:0;" alt="PayPal" />';
	        $str .= '</form>';
		}
		return $str;
	}

}

?>