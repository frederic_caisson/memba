<?php
class AdminController extends Controller
{
	public function __construct() {
		parent::__construct();
        $this->adminMemberController = new AdminMemberController();
        add_action( 'wp_ajax_delete_member', array( $this->adminMemberController , 'deleteMember' ) );

        $this->adminPageController = new AdminPageController();
        add_action( 'wp_ajax_delete_page', array( $this->adminPageController , 'deletePage' ) );
        add_action( 'wp_ajax_create_page', array( $this->adminPageController , 'createPage' ) );

        $this->adminProductController = new AdminProductController();
        $this->adminPaymentController = new AdminPaymentController();

    	add_action('admin_menu', array($this,'addMenu'));

    	$this->menu_dashboard = array('slug' => THIS_PLUGIN_NAME.'_admin_dashboard',
    								  'label' => ucfirst(THIS_PLUGIN_NAME),
    									);
        $this->menu_members = array('slug' => THIS_PLUGIN_NAME.'_admin_members',
    								'label' => __('Members List', THIS_PLUGIN_NAME),
    									);
        $this->menu_pages = array('slug' => THIS_PLUGIN_NAME.'_admin_pages',
    							  'label' => __('Pages Settings', THIS_PLUGIN_NAME),
    									);
        $this->menu_products = array('slug' => THIS_PLUGIN_NAME.'_admin_products',
                                    'label' => __('Products List', THIS_PLUGIN_NAME),
                                        );
        $this->menu_payment = array('slug' => THIS_PLUGIN_NAME.'_admin_payment',
                                    'label' => __('Payment Settings', THIS_PLUGIN_NAME),
                                        );

        wp_enqueue_script('tablesorter-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/tablesorter.js');
        
	}

	
	public function pages(){

		//$this->adminPageController = new AdminPageController();
		$this->adminPageController->listPages();
		
	}

    public function products(){

        $this->adminProductController->setData('menu_products', $this->menu_products);
        if(isset($_GET['edit'])){

            $productId = $_GET['edit'];
            $this->adminProductController->editProduct($productId);
        
        } else if(isset($_GET['create'])){

            $this->adminProductController->createProduct();

        } else {
            
            $this->adminProductController->listProducts();
        } 
    }

    public function dashboard(){
    
    }

    public function members(){
        
        $this->adminMemberController->setData('menu_members', $this->menu_members);
    	if(isset($_GET['edit'])){

            $userId = $_GET['edit'];
            $this->adminMemberController->editMember($userId);
        
    	} else if(isset($_GET['create'])){

            $this->adminMemberController->createMember();

        } else {
    		
    		$this->adminMemberController->listMembers();
    	}
    }

    public function payment(){
        $this->adminPaymentController->configurePayment();
    }


    public function addMenu() {

        // REF : add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
        $bubblePageSettings ='';
        $pageModel = new Page();
        $pageSettings = $pageModel->checkPageSettings();
        if($pageSettings['result'] == 0) {
            $bubblePageSettings = " <span class='update-plugins count-1'><span class='update-count'><i class='fa fa-exclamation'></i></span></span>";
        }

        add_menu_page($this->menu_dashboard['label'], $this->menu_dashboard['label'].$bubblePageSettings, 'manage_options', $this->menu_dashboard['slug'], array($this,'dashboard'),'dashicons-groups');

        add_submenu_page( $this->menu_dashboard['slug'], $this->menu_members['label'], $this->menu_members['label'], 'manage_options', $this->menu_members['slug'], array($this,'members'));

        add_submenu_page( $this->menu_dashboard['slug'], $this->menu_products['label'], $this->menu_products['label'], 'manage_options', $this->menu_products['slug'], array($this,'products'));

        add_submenu_page( $this->menu_dashboard['slug'], $this->menu_pages['label'], $this->menu_pages['label'].$bubblePageSettings, 'manage_options', $this->menu_pages['slug'] , array($this,'pages'));
    
        add_submenu_page( $this->menu_dashboard['slug'], $this->menu_payment['label'], $this->menu_payment['label'], 'manage_options', $this->menu_payment['slug'], array($this,'payment'));

    }
}
?>