<?php

class PublicMemberController extends Controller
{
	function deleteMember(){
		$response = array('result' => 1, 'message'=> __('Deleted',THIS_PLUGIN_NAME));
		
		echo json_encode($response);
		die();
	}

	public function saveMember(){
		$userModel = new User();
		$user = Session::getConnectedUser();
		$user->email = $_POST['email'];
		$user->password = $_POST['password'];
		$user->confirm = $_POST['confirm'];
		$user->first_name = $_POST['first_name'];
		$user->last_name = $_POST['last_name'];
		$user->postal = $_POST['postal'];
		$user->address1 = $_POST['address1'];
		$user->address2 = $_POST['address2'];
		$user->tel = $_POST['tel'];

		$response = $userModel->valid($user);

		if($response['result'] == 1){
			$userModel->save($user);
			$response = array('result' => 1, 'message'=> __('Saved',THIS_PLUGIN_NAME));
		} 
		
		echo json_encode($response);
		die();
	}
}

?>