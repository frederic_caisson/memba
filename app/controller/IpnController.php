<?php

class IpnController extends Controller
{
	//https://developer.paypal.com/docs/classic/api/merchant/RefundTransaction_API_Operation_NVP/
	//http://www.smashingmagazine.com/2011/09/05/getting-started-with-the-paypal-api/
	const API_VERSION = 117;

    private function log($invoiceId, $status,  $log, $data){
    	$invoiceLog = new StdClass();
    	$invoiceLog->invoice_id = $invoiceId;
    	$invoiceLog->status = $status;
    	$invoiceLog->log = $log;
    	$invoiceLog->data = $data;
    	$invoiceLogModel = new InvoiceLog();
    	$invoiceLogModel->log($invoiceLog);
    	unset($invoiceLog);
    	unset($invoiceLogModel);
    }

	private function valid($post, $config, $product){

		//Check email		
		if($config->email != $post['receiver_email']) {
			return false;
		}

		//Check Product
		if($product){

			$dbSellPrice = floatval($product->sell_price);
			$dbBillingPeriod = $product->billing_interval.' '.$product->billing_period;
			$dbTrialPrice = floatval($product->trial_price);
			$dbTrialPeriod = $product->trial_interval.' '.$product->trial_period;

			if($product->currency != $post['mc_currency']){
				return false;
			}

			switch($product->type){
				default:
					return false;
					break;
				case 1:

					$amount = floatval($post['mc_gross']);
					if($amount != $dbSellPrice){
						
						return false;
					}
					if($post['payment_status'] != 'Completed'){
						
						return false;
					}
					break;
				case 2:
				    
					$amount = floatval($post['mc_gross']);
					if($amount != $dbSellPrice){

						return false;
					}
					$amount = floatval($post['mc_amount3']);
					if($amount != $dbSellPrice){
						return false;
					}
					if($post['period3'] != $dbBillingPeriod){
						return false;
					}
					break;
				case 3:
				   
				    $trial = floatval($post['mc_amount1']);
					if($trial != $dbTrialPrice){

						return false;
					}
					if($post['period1'] != $dbTrialPeriod){

						return false;
					}
					$amount = floatval($post['mc_amount3']);
					if($amount != $dbSellPrice){

						return false;
					}
					if($post['period3'] != $dbBillingPeriod){
						return false;
					}
					break;
				case 4:
				    
				    if(intval($post['recurring']) != 1){
						return false;
					}
					$amount = floatval($post['mc_amount3']);
					if($amount != $dbSellPrice){
						return false;
					}
					if($post['period3'] != $dbBillingPeriod){
						return false;
					}
					break;
				case 5:
				    
					$trial = floatval($post['mc_amount1']);
					if($trial != $dbTrialPrice){
						return false;
					}
					if($post['period1'] != $dbTrialPeriod){
						return false;
					}
					$amount = floatval($post['mc_amount3']);
					if($amount != $dbSellPrice){
						return false;
					}
					if($post['period3'] != $dbBillingPeriod){
						return false;
					}
					break;
			}
		}

		return true;
	}

	public function forValid($post,$product){

		switch($product->type){
			case 1:
				if($post['txn_type'] == 'web_accept'){
					return true;
				}
				break;
			case 2:
				if($post['txn_type'] == 'subscr_signup'){
					return true;
				}
				break;
			case 3:
				if($post['txn_type'] == 'subscr_signup'){
					return true;
				}
				break;
			case 4:
				if($post['txn_type'] == 'subscr_signup'){
					return true;
				}
				break;
			case 5:
				if($post['txn_type'] == 'subscr_signup'){
					return true;
				}
				break;	
		}

		return false;

	}

	public function getConfig(){
		$configModel = new Config();
		$config = $configModel->findByKey(Config::PAYMENT_PAYPAL_KEY);
		unset($configModel);
		if ($config) {
			if($config->mode == 1) {
				$config->url = 'paypal';
				$config->email = $config->paypal_email;
			} else {
				$config->url = 'sandbox.paypal';
				$config->email = $config->paypal_email_sandbox;
			}
		}
		return $config;
	}

	public function notify(){

		//Check if the request is a paypal request
		if(!isset($_POST['txn_type']) || !isset($_POST['invoice'])){
			echo __('Bad request');
			exit();
		}

		parse_str($_POST['custom'],$custom);
		$productModel = new Product();
		$product = $productModel->findById($custom['product_id']);
		if(!$product){
			$this->log($_POST['invoice'], InvoiceLog::STATUS_ERROR, __('Product not found', THIS_PLUGIN_NAME), json_encode($data));
			exit();
		}
		unset($productModel);
		
		$config = $this->getConfig();

		if (!$config) {
			$this->log($_POST['invoice'], InvoiceLog::STATUS_ERROR, __('Configuration not found', THIS_PLUGIN_NAME), json_encode($data));
			exit();
		}
		
		$req = 'cmd=_notify-validate';
		foreach ($_POST as $key => $value) {
			$value = urlencode(stripslashes($value));
			$req .= "&$key=$value";
		}
		 
		$header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
		$header .= "Host: www.".$config->url.".com\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
		$fp = fsockopen ("ssl://www.".$config->url.".com", 443, $errno, $errstr, 30);

		if (!$fp) {

		 	$this->log($_POST['invoice'], InvoiceLog::STATUS_ERROR, __('Could not connect to ', THIS_PLUGIN_NAME).$config->url.".com", json_encode($_POST));
		
		} else {
			fputs ($fp, $header . $req);
			while (!feof($fp)) {
				$res = fgets ($fp, 1024);

				if (strcmp ($res, 'VERIFIED') == 0) {
					$this->log($_POST['invoice'], InvoiceLog::STATUS_INFO, __('Info', THIS_PLUGIN_NAME), json_encode($_POST));

					if($this->forValid($_POST, $product)){
						
						if($this->valid($_POST, $config, $product)){

							$userModel = new User();
							$user = $userModel->findByEmail($_POST['payer_email']);

							if(!$user){
								$userId = false;
								$user = new StdClass();
								$user->email = $_POST['payer_email'];
								$user->first_name = $_POST['first_name'];
								$user->last_name = $_POST['last_name'];
								$user->password = Util::randomPassword(10);
							} else {
								$userId = $user->id;
							}

							$userProductModel = new UserProduct();
							if(!$userId){

								$userId = $userModel->save($user);

							} else {

								//Check if already subscribed
								$check = $userProductModel->checkSubscribed($userId, $product->id);

								if( $check ){
									//send error mail

									$this->log($_POST['invoice'], InvoiceLog::STATUS_ERROR, __('Already subscribed', THIS_PLUGIN_NAME), json_encode($_POST));
									if($this->refund($_POST['txn_id'], $_POST['invoice'], $_POST['payer_id'])){
										$this->log($_POST['invoice'], InvoiceLog::STATUS_INFO, __('Refund', THIS_PLUGIN_NAME), json_encode($_POST));
									}
									exit();
								}
							}

							$userProduct = new StdClass();
							$userProduct->user_id = $userId;
							$userProduct->date_start = date('Y-m-d');
							$userProduct->date_end = null;
							$userProduct->product_id = $product->id;
							$userProduct->sell_price = $product->sell_price;
							$userProduct->currency = $product->currency;
							$userProduct->canceled = null;
							$userProduct->validated = 1;
							$userProduct->trial_price = $product->trial_price;
							$userProduct->trial_period = $product->trial_period;
							$userProduct->trial_interval = $product->trial_interval;
							$userProduct->sell_price = $product->sell_price;
							$userProduct->billing_period = $product->billing_period;
							$userProduct->billing_interval = $product->billing_interval;
							$userProduct->billing_cycles = $product->billing_cycles;
							$userProduct->tax = $product->tax;
							$userProduct->shipping_fees = $product->shipping_fees;
							$userProduct->quantity = $product->quantity;
							$userProduct->shipping_fees = $product->shipping_fees;
							$userProduct->coupon_id = null;
							$userProduct->invoice_id = $_POST['invoice'];
							$userProductModel->insert($userProduct);
							$this->log($_POST['invoice'], InvoiceLog::STATUS_VALIDATED, __('Validated', THIS_PLUGIN_NAME), json_encode($_POST));

						} else {
							//send error mail
							$this->log($_POST['invoice'], InvoiceLog::STATUS_ERROR, __('Invalid data', THIS_PLUGIN_NAME), json_encode($_POST));
						}

					}
					exit();
					
				} else if (strcmp ($res, 'INVALID') == 0) {
					$this->log($_POST['invoice'], InvoiceLog::STATUS_ERROR, __('Paypal verification failed', THIS_PLUGIN_NAME), json_encode($_POST));
				}
			}
			fclose ($fp);
		}
			
	}

	public function refund($transctionId, $payerId, $invoiceId){
		$config = $this->getConfig();

		if (!$config) {
			exit();
		}
		$url = 'https://api-3t.'.$config->url.'.com/nvp';
		$nvp = array(
			'TRANSACTIONID'	=> $transctionId,
			'PAYERID'	=> $payerId,
			'INVOICEID'	=> $invoiceId,
			'REFUNDTYPE'=> 'Full',
			'METHOD'	=> 'RefundTransaction',
			'VERSION'	=> self::API_VERSION,
			'USER'	=> $config->paypal_api_username,
			'PWD'	=> $config->paypal_api_password,
			'SIGNATURE'	=> $config->paypal_api_signature,
			'API_ENDPOINT' => $url,
		);
		var_dump($nvp);
		$curl = curl_init();
		curl_setopt( $curl , CURLOPT_URL , $url ); 
		curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false );
		curl_setopt( $curl , CURLOPT_RETURNTRANSFER , 1 );
		curl_setopt( $curl , CURLOPT_POST , 1 );
		curl_setopt( $curl , CURLOPT_POSTFIELDS , http_build_query( $nvp ) );
		$response = urldecode( curl_exec( $curl ) );

		curl_close( $curl );
		$responseArray = array();
        parse_str($response,$responseArray);

        var_dump($responseArray);

        if($responseArray['REFUNDINFO']['REFUNDSTATUS'] != 'none'){
        	return true;
        } else {
        	return false;
        }
	}

	public function thank(){
		$this->refund('67D75257VK221111S','MXHNW4JNPUUW8','1411123176-1673681179-2');
	}

	public function cancel($post){

	}
}

?>