<?php
class PublicPaypalController extends Controller
{
	CONST API_VERSION = 117;

	public function ipn(){

	}

    public function checkout(){

    	wp_enqueue_script('tz-script', '//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js');
    	
    	if(isset($_POST['product_id'])){
    		$productModel = new Product();
    		$product = $productModel->findById($_POST['product_id']);
    		$agreement = $productModel->getBillingAgreement($product);
    		$param = THIS_PLUGIN_NAME.'_process';
			$url = site_url().'/?'.$param.'='.$_POST['product_id'];

			wp_enqueue_script('checkout-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/checkout.js',array( 'jquery' ));
    		wp_localize_script( 'checkout-script', 'ajax_object', 
			                    array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 
			                    	  'error_all_null' => __('If you already have an account sign in with your member account.<br>Or enter your paypal account to create a new account.', THIS_PLUGIN_NAME),
			                    	  'ajax_url' => $url));
    		if($product){
    			$user = Session::getConnectedUser();
    			$view = 'user_checkout';
	    		$params = array('product' => $product,
	    			            'agreement' => $agreement,
	    			            'user' => $user);	
	    		$this->renderView($view,$params);
    		}
    	}
    }

    public function setExpressCheckout($config, $email, $timezone, $product){
    	
    	$params = array('product_id' => $product->id, 'timezone' => $timezone);
    	$params = json_encode($params);
    	$encoded = base64_encode($params);

    	$post = array();
    	$post ["VERSION"] = self::API_VERSION;
		$post ["USER"] = $config->user;
		$post ["PWD"] = $config->password;
		$post ["SIGNATURE"] = $config->signature;
		$post ["API_ENDPOINT"] = 'https://api-3t.'.$config->domain.'.com/nvp';

		$param = THIS_PLUGIN_NAME.'_thank';
		$url = site_url().'/?'.$param.'='.$encoded;
		$post ["RETURNURL"] = $url;

		$param = THIS_PLUGIN_NAME.'_cancel';
		$url = site_url().'/?'.$param.'='.$encoded;
		$post ["CANCELURL"] = $url;
		$post ["METHOD"] = 'SetExpressCheckout';
		if(!empty($email)){
			$post ["EMAIL"] =  $email;
		}

    	if ($product->type <= 2) {
			$post ["PAYMENTREQUEST_0_TAXAMT"] = $product->tax;
			$post ["PAYMENTREQUEST_0_SHIPPINGAMT"] = $product->shipping_fees;
			$post ["L_PAYMENTREQUEST_0_NAME0"] = $product->name;
			$post ["L_PAYMENTREQUEST_0_DESC0"]= $product->plan;
	        $post ["L_PAYMENTREQUEST_0_AMT0"]= $product->sell_price;
	        $post ["L_PAYMENTREQUEST_0_QTY0"]= 1;
	        $post ["PAYMENTREQUEST_0_AMT"] = ($product->sell_price + $product->tax + $product->shipping_fees);
	        $post ["PAYMENTREQUEST_0_CURRENCYCODE"]= $product->currency;
    	} else {
			$post ["L_BILLINGTYPE0"] = 'RecurringPayments';
    		$post ["L_BILLINGAGREEMENTDESCRIPTION0"] = $product->plan;
    	}

    	return $post;
    }

    public function getExpressCheckoutDetails($config, $token){
    	$post = array();
    	$post ["VERSION"] = self::API_VERSION;
		$post ["USER"] = $config->user;
		$post ["PWD"] = $config->password;
		$post ["SIGNATURE"] = $config->signature;
		$post ["API_ENDPOINT"] = 'https://api-3t.'.$config->domain.'.com/nvp';
		$post ["METHOD"] = 'GetExpressCheckoutDetails';
		$post ["TOKEN"] = $token;

		return $post;
    }

    public function doExpressCheckoutPayment($config, $param){
    	$post = array();
    	$post ["VERSION"] = self::API_VERSION;
		$post ["USER"] = $config->user;
		$post ["PWD"] = $config->password;
		$post ["SIGNATURE"] = $config->signature;
		$post ["API_ENDPOINT"] = 'https://api-3t.'.$config->domain.'.com/nvp';
		$post ["METHOD"] = 'DoExpressCheckoutPayment';
		$post ["TOKEN"] = $param['TOKEN'];
		$post ["PAYERID"] = $param['PAYERID'];
		$post ["PAYMENTREQUEST_0_PAYMENTACTION"] = 'Sale';
		$post ["PAYMENTREQUEST_0_AMT"] = $param['PAYMENTREQUEST_0_AMT'];
		$post ["PAYMENTREQUEST_0_CURRENCYCODE"] = $param['PAYMENTREQUEST_0_CURRENCYCODE'];
		$post ["PAYMENTREQUEST_0_TAXAMT"] = $param['PAYMENTREQUEST_0_TAXAMT'];
		$post ["PAYMENTREQUEST_0_SHIPPINGAMT"] = $param['PAYMENTREQUEST_0_SHIPPINGAMT'];

		return $post;
    }

    public function createRecurringPaymentsProfile($config, $product, $param){

    	$paypalPeriodModel = new PaypalPeriod();

    	$post = array();
    	$post ["VERSION"] = self::API_VERSION;
		$post ["USER"] = $config->user;
		$post ["PWD"] = $config->password;
		$post ["SIGNATURE"] = $config->signature;
		$post ["API_ENDPOINT"] = 'https://api-3t.'.$config->domain.'.com/nvp';
		$post ["METHOD"] = 'CreateRecurringPaymentsProfile';
		$post ["TOKEN"] = $param['TOKEN'];
		$post ["PAYERID"] = $param['PAYERID'];
		$post ["EMAIL"] =  $param['EMAIL'];
		//get current gmdate
		$gmdate = gmdate("Y-m-d H:i:s"); 
		//add 30 min to have more time to process the transaction
		$newtimestamp = strtotime("$gmdate + 30 minute");
		$post ["PROFILESTARTDATE"] = date('Y-m-d\TH:i:s\Z', $newtimestamp);
		$post ["DESC"] = $product->plan;
		$post ["BILLINGPERIOD"] = $paypalPeriodModel->determinateNvpPeriod($product->billing_period);
		$post ["BILLINGFREQUENCY"] = $product->billing_interval;
		$post ["TOTALBILLINGCYCLES"] = $product->billing_cycles;
		$post ["AMT"] = $product->sell_price;

		if($product->type == 4 || $product->type == 6){
			$post ["TRIALBILLINGPERIOD"] = $paypalPeriodModel->determinateNvpPeriod($product->trial_period);
			$post ["TRIALBILLINGFREQUENCY"] = $product->trial_interval;
			$post ["TRIALAMT"] = $product->trial_price;
			$post ["TRIALTOTALBILLINGCYCLES"] = 1;
		}

		$post ["CURRENCYCODE"] = $product->currency;
		if(isset($product->shipping_fees)){
			$post ["SHIPPINGAMT"] = $product->shipping_fees;
		}
		if(isset($product->tax)){
			$post ["TAXAMT"] = $product->tax;
		}

		return $post;
	}

	public function checkUserProduct($user,$product){
		$userProductModel = new UserProduct();
		$subscribed = $userProductModel->checkSubscribed($user, $product);

		if($subscribed){
			$response = array('result' => 0, 'message'=> __('You already bought this product',THIS_PLUGIN_NAME));
		} else {
			$response = array('result' => 1, 'message'=> __('Can suscribed',THIS_PLUGIN_NAME));
		}
		return $response;
	}

	public function checkUser($post){
		if(empty($post['email']) && empty($post['password']) && empty($post['paypal_email'])){
			$response = array('result' => 0, 'message'=> __('If you already have an account sign in with your member account.<br>Or enter your paypal account to create a new account.',THIS_PLUGIN_NAME));
			return $response;
		}

		$response = array('result' => 0, 'message'=> __('Invalid',THIS_PLUGIN_NAME), 'p' => $post);
		$userModel = new User();
		if(!empty($post['email']) && !empty($post['password'])){
			$email = $post['email'];
			$password = $post['password'];
			$user = $userModel->findByEmail($email);
			if ($user) {
				if($user->password == $password) {
	        		Session::createSession($user->id);
					$response = array('result' => 1, 'message'=> __('User logged',THIS_PLUGIN_NAME));
				} else {
					$response = array('result' => 0, 'message'=> __('Entered password is invalid',THIS_PLUGIN_NAME));
				}
			} else {
				$response = array('result' => 0, 'message'=> __('This email is not registred',THIS_PLUGIN_NAME));
			}
		} else if (!empty($post['paypal_email'])) {
			$paypal_email = $post['paypal_email'];
			$user = $userModel->findByPaypalEmail($paypal_email);
			if ($user) {
				$response = array('result' => 0, 'message'=> __('This paypal account is already registred.<br>Sign in with your member account.',THIS_PLUGIN_NAME));
			} else {
				$response = array('result' => 1, 'message'=> __('New user',THIS_PLUGIN_NAME));
			}
		}
		return $response;
	}

	public function process(){

		$response = array('result' => 0, 'message'=> __('Invalid transaction',THIS_PLUGIN_NAME));

		if (isset($_POST['product_id'])){
			$productModel = new Product();
			$product = $productModel->findById($_POST['product_id']);

			if ($product){

				$user = Session::getConnectedUser();
				if($user){
					$email = $user->paypal_email;
					$response = $this->checkUserProduct($user, $product);
				} else {
					$response = $this->checkUser($_POST);

					$user = Session::getConnectedUser();
					if($user){
						$response = $this->checkUserProduct($user, $product);
					}
				}	
				
				if ($response['result'] == 1){
					$config = $this->getConfig();
		    		$product->plan = $productModel->getBillingAgreement($product);
		    		$timezone = $_POST['timezone'];

		    		if(!$user){
			    		$userModel = new User();
			    		if(!empty($_POST['email'])){
			    			$user = $userModel->findByEmail($email);
			    			$email = $user->paypal_email;
			    		} else {
			    			$email = $_POST['paypal_email'];
			    		}
		    		}
		    		
	    			$post = $this->setExpressCheckout($config, $email, $timezone, $product);
	    			$response = $this->scrap($post);

	    			if ("SUCCESS" == strtoupper($response["ACK"])){
	    				$paypalurl ='https://www.'.$config->domain.'.com/cgi-bin/webscr?cmd=_express-checkout&token='.$response["TOKEN"];
	                	$response = array('result' => 1, 'message'=> __('Success',THIS_PLUGIN_NAME), 'redirect' => $paypalurl);
	    			} else {
	    				$response = array('result' => 0, 'message'=> __('An error occured. Retry your transaction later.',THIS_PLUGIN_NAME));
	    			}
				}
			}
		}
		echo json_encode($response);
		die();
	}

	public function cancel(){

	}

	public function thank(){

		if(isset($_GET['token']) && isset($_GET[THIS_PLUGIN_NAME.'_thank'])){

			//get parameters
			$decoded = base64_decode($_GET[THIS_PLUGIN_NAME.'_thank']);
			$params = json_decode($decoded,true);

			$productModel = new Product();
    		$product = $productModel->findById($params['product_id']);
    		$invoiceId = InvoiceLog::generateInvoiceId($product);

    		if(Util::isValidTimezone($params['timezone'])){
    			$date = new DateTime("now", new DateTimeZone($params['timezone']));
    			$timezone = $params['timezone'];
    		} else {
    			$date =  new DateTime(gmdate("Y-m-d H:i:s"));
    			$timezone = '';
    		}

    		if($product){
    			$product->plan = $productModel->getBillingAgreement($product);
    			//get express response
				$config = $this->getConfig();
				$post = $this->getExpressCheckoutDetails($config, $_GET['token']);
				$response = $this->scrap($post);

				if("SUCCESS" == strtoupper($response["ACK"])){

					//prepare user object
					$user = new StdClass();
					$user->id = null;
					$user->email = $response['EMAIL'];
					$user->paypal_email = $response['EMAIL'];
					$user->first_name = $response['FIRSTNAME'];
					$user->last_name = $response['LASTNAME'];
					$user->password = Util::randomPassword(10);
					$user->postal = $response['SHIPTOZIP'];
					$user->address1 = $response['SHIPTOSTREET'];
					$user->address2 = trim($response["SHIPTOSTATE"].' '.$response['SHIPTOCITY']);
					$user->timezone = $timezone;

					//if product one time type
					if($product->type <= 2){
						$post = $this->doExpressCheckoutPayment($config,$response);
						$response = $this->scrap($post);
						$log = __('Payment completed');
					} else {
						$post = $this->createRecurringPaymentsProfile($config, $product, $response);
						$response = $this->scrap($post);
						$log = __('Subscription completed');
					}


					if("SUCCESS" == strtoupper($response["ACK"])){
						//find or create user
						$userModel = new User();
						$existUser = $userModel->findByPaypalEmail($user->paypal_email);

						if(empty($existUser)){
							$userModel = new User();
							$userId = $userModel->save($user);
						} else {
							$userId = $existUser->id;
						}

						if($userId > 0){
							$this->attachUserAndProduct($invoiceId, 
														$date, 
														$userId,
														$product);

					    	$this->log($invoiceId,
					    		       InvoiceLog::STATUS_VALIDATED,
					    		       $log,
					    		       $response);
					    	
							$view = 'user_thank';
			    			$this->renderView($view);
		    			}
	    			}
				}
			}
		}
	}

	private function attachUserAndProduct($invoice_id, $date, $user_id, $product){

		$date_end = Model::NULL;
		if($product->type == 2 || $product->type > 3){
			
			$cycle = intval($product->billing_cycles);
			if($cycle == 0){
				$cycle = 1;
			}
			$interval = $cycle * $product->billing_interval;
			$date_end = Util::addPeriod($date, $interval, $product->billing_period);

			if(intval($product->trial_interval) > 0){
				$date_end = Util::addPeriod($date_end, $product->trial_interval, $product->trial_period);
			}

			$date_end = $date_end->format('Y-m-d H:i:s');
		}  

		$userProduct = new StdClass();
		$userProduct->user_id = $user_id;
		$userProduct->date_start = $date->format('Y-m-d H:i:s');
		$userProduct->date_end = $date_end;
		$userProduct->product_id = $product->id;
		$userProduct->sell_price = $product->sell_price;
		$userProduct->currency = $product->currency;
		$userProduct->canceled = null;
		$userProduct->validated = 1;
		$userProduct->trial_price = $product->trial_price;
		$userProduct->trial_period = $product->trial_period;
		$userProduct->trial_interval = $product->trial_interval;
		$userProduct->sell_price = $product->sell_price;
		$userProduct->billing_period = $product->billing_period;
		$userProduct->billing_interval = $product->billing_interval;
		$userProduct->billing_cycles = $product->billing_cycles;
		$userProduct->tax = $product->tax;
		$userProduct->shipping_fees = $product->shipping_fees;
		$userProduct->quantity = $product->quantity;
		$userProduct->shipping_fees = $product->shipping_fees;
		$userProduct->coupon_id = null;
		$userProduct->invoice_id = $invoice_id;
		$userProductModel = new UserProduct();
		$userProductModel->insert($userProduct);
		unset($userProduct);
		unset($userProductModel);
	}

	private function log($invoice_id, $status, $log, $data){
		$invoiceLog = new StdClass();
		$invoiceLog->invoice_id = $invoice_id; 
    	$invoiceLog->status = $status; 
    	$invoiceLog->log = $log;
    	$invoiceLog->data = json_encode($data);
		$invoiceLogModel = new InvoiceLog();
		$invoiceLogModel->log($invoiceLog);
		unset($invoiceLog);
		unset($invoiceLogModel);
	}

	private function scrap($param){
		$curl = curl_init();
		curl_setopt( $curl , CURLOPT_URL , $param["API_ENDPOINT"] ); 
		curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false );
		curl_setopt( $curl , CURLOPT_SSL_VERIFYHOST , false );
		curl_setopt( $curl , CURLOPT_RETURNTRANSFER , 1 );
		curl_setopt( $curl , CURLOPT_POST , 1 );
		curl_setopt( $curl , CURLOPT_POSTFIELDS , http_build_query( $param ) );
		$response = urldecode( curl_exec( $curl ) );
		curl_close( $curl );
		unset($curl);
		$responseArray = array();
        parse_str($response,$responseArray);
        return $responseArray;
	}

	private function getConfig(){
		$configModel = new Config();
		$config = $configModel->findByKey(Config::PAYMENT_PAYPAL_KEY);
		unset($configModel);
		if ($config) {
			if($config->mode == 1) {
				$config->domain = "paypal";
				$config->email = $config->paypal_email;
				$config->user = $config->paypal_api_username;
				$config->password = $config->paypal_api_password;
				$config->signature = $config->paypal_api_signature;
			} else {
				$config->domain = "sandbox.paypal";
				$config->email = $config->paypal_email_sandbox;
				$config->user = $config->paypal_api_username_sandbox;
				$config->password = $config->paypal_api_password_sandbox;
				$config->signature = $config->paypal_api_signature_sandbox;
			}
		}
		return $config;
	}

}
?>