<?php
class AdminPaymentController extends Controller
{
	public function configurePayment(){
		wp_enqueue_script('admin-payment-script', THIS_PLUGIN_BASE_DIR_HTTP.'/js/admin-payment.js',array( 'jquery' ));
		$configModel = new Config();

		$config = $configModel->findByKey(Config::PAYMENT_PAYPAL_KEY);

		if(isset($_POST['submit'])){
			$config = new StdClass();

			$config->mode = $_POST['mode'];
			$config->paypal_email = trim($_POST['paypal_email']);
			$config->paypal_api_username = trim($_POST['paypal_api_username']);
			$config->paypal_api_password = trim($_POST['paypal_api_password']);
			$config->paypal_api_signature = trim($_POST['paypal_api_signature']);

			$config->paypal_email_sandbox = trim($_POST['paypal_email_sandbox']);
			$config->paypal_api_username_sandbox = trim($_POST['paypal_api_username_sandbox']);
			$config->paypal_api_password_sandbox = trim($_POST['paypal_api_password_sandbox']);
			$config->paypal_api_signature_sandbox = trim($_POST['paypal_api_signature_sandbox']);

			$array = array ( 'mode' => $_POST['mode'],
							 'paypal_email' => trim($_POST['paypal_email']),
							 'paypal_api_username' => trim($_POST['paypal_api_username']),
							 'paypal_api_password' => trim($_POST['paypal_api_password']),
							 'paypal_api_signature' => trim($_POST['paypal_api_signature']),
							 'paypal_email_sandbox' => trim($_POST['paypal_email_sandbox']),
							 'paypal_api_username_sandbox' => trim($_POST['paypal_api_username_sandbox']),
							 'paypal_api_password_sandbox' => trim($_POST['paypal_api_password_sandbox']),
							 'paypal_api_signature_sandbox' => trim($_POST['paypal_api_signature_sandbox']),

			);
			
			$json = json_encode($array);

			$configModel->save(Config::PAYMENT_PAYPAL_KEY,$json);

		} else {
			
			if(!$config){
				$config = new StdClass();
				$config->mode = 0;
			}
		}
		
		$params = array('config' => $config,
						);

		$includeViews[] = 'admin_menu';
		$view = 'admin_payment';
		$this->renderView($view,$params,$includeViews);
	}
}
?>