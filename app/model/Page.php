<?php
class Page extends Model{

	const PAGE_LOGIN_SHORTCODE = 'login_page';
	const PAGE_DASHBOARD_SHORTCODE = 'dashboard_page';

	public function valid($items){
		$response = array('result' => 1, 'OK');
	}

	public function save($slug,$page_id) {

		if(empty($page_id)) {
			$page_id = null;
		}

		$result = $this->wpdb->insert( 
			THIS_PLUGIN_NAME.'_page', 
			array( 
				'slug' => $slug,
				'page_id' => $page_id
			), 
			array( 
				'%s',
				'%s',
			)			
		);

		return $result;
	}

	public function delete($page_id) {

		return $this->wpdb->delete( THIS_PLUGIN_NAME.'_page', array( 'page_id' => $page_id ) );

	}

	public function findBySlug($slug) {
		$sql = "SELECT page_id FROM ".THIS_PLUGIN_NAME."_page WHERE slug = '$slug'";

		$result = $this->wpdb->get_row($sql);

		if($result) {
			return $result->page_id;
		} 
		return $result;
	}

	public function checkPageSettings(){
		$messages = array();
		$response = array('result' => 1, 'messages' => $messages);
		if(!$this->findBySlug(self::PAGE_LOGIN_SHORTCODE)){
			$messages[] = __("Please create the Login page",THIS_PLUGIN_NAME);
			$response = array('result' => 0, 'messages' => $messages);
		}
		if(!$this->findBySlug(self::PAGE_DASHBOARD_SHORTCODE)){
			$messages[] = __("Please create the User's dashboard page",THIS_PLUGIN_NAME);
			$response = array('result' => 0, 'messages' => $messages);
		}

		return $response;

	}
	
}

?>