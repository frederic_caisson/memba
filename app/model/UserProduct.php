<?php
class UserProduct extends Model{

	public function findUserProducts($user) {
		$userId = $user->id;
		$timezone = $user->timezone;
		$date = new DateTime('now', new DateTimeZone($timezone) );
		$strDate = $date->format('Y-m-d');

		$sql   = "SELECT memba_product.id, memba_product.name, memba_user_product.date_start";
		$sql  .= " FROM memba_user_product, memba_product";
		$sql  .= " WHERE memba_user_product.product_id = memba_product.id";
		$sql  .= " AND memba_user_product.user_id = $userId";
		$sql  .= " AND memba_user_product.is_cancel = 0";
		$sql  .= " AND (memba_user_product.date_end >= '$strDate' OR memba_user_product.date_end IS NULL)";
		$sql  .= " GROUP BY memba_product.id ORDER BY memba_product.name";		
		$products = $this->wpdb->get_results($sql);

		return $products;
	}

	public function findByKey($userProduct){
		$sql = "SELECT * FROM ".THIS_PLUGIN_NAME."_user_product WHERE user_id=".$userProduct->user_id;
		$sql .= " AND product_id=".$userProduct->product_id." AND date_start='".$userProduct->date_start."'";
		$row = $this->wpdb->get_row($sql);

		return $row;
	}

	public function checkSubscribed($user, $product){
		if(Util::isValidTimezone($user->timezone)){
			$date = new DateTime("now", new DateTimeZone($user->timezone));
		} else {
			$date =  new DateTime(gmdate("Y-m-d H:i:s"));
		}
		$date_str = $date->format('Y-m-d H:i:s');

		$sql = "SELECT * FROM ".THIS_PLUGIN_NAME."_user_product WHERE user_id=".$user->id;
		$sql .= " AND product_id=".$product->id." AND date_end > '".$date_str."' AND canceled=0 AND validated=1";

		$row = $this->wpdb->get_row($sql);

		if($row){
			return true;
		} else {
			return false;
		}

	}

	public function insert($userProduct){
		if(empty($userProduct->date_end)) {
			$userProduct->date_end = self::NULL;
		}
		if(empty($userProduct->quantity)) {
			$userProduct->quantity = self::NULL;
		}
		if(empty($userProduct->canceled)) {
			$userProduct->canceled = 0;
		}
		if(empty($userProduct->validated)) {
			$userProduct->validated = 0;
		}
		if(empty($userProduct->trial_price)) {
			$userProduct->trial_price = self::NULL;
		}
		if(empty($userProduct->trial_period)) {
			$userProduct->trial_period = self::NULL;
		}
		if(empty($userProduct->trial_interval)) {
			$userProduct->trial_interval = self::NULL;
		}
		if(empty($userProduct->billing_period)) {
			$userProduct->billing_period = self::NULL;
		}
		if(empty($userProduct->billing_interval)) {
			$userProduct->billing_interval = self::NULL;
		}
		if(empty($userProduct->billing_cycles)) {
			$userProduct->billing_cycles = self::NULL;
		}
		if(empty($userProduct->shipping_fees)) {
			$userProduct->shipping_fees = self::NULL;
		}
		if(empty($userProduct->tax)) {
			$userProduct->tax = self::NULL;
		}

		if(empty($userProduct->quantity)) {
			$userProduct->quantity = self::NULL;
		}
		if(empty($userProduct->coupon_id)) {
			$userProduct->coupon_id = self::NULL;
		}
		
		
		$result = false;
		
		$result = $this->wpdb->insert( 
			THIS_PLUGIN_NAME.'_user_product', 
			array( 
				'user_id' => $userProduct->user_id,
				'product_id' => $userProduct->product_id,
				'date_start' => $userProduct->date_start,
				'date_end' => $userProduct->date_end,
				'currency' => $userProduct->currency,
				'canceled' => $userProduct->canceled,
				'validated' => $userProduct->validated,
				'trial_price' => $userProduct->trial_price,
				'trial_period' => $userProduct->trial_period,
				'trial_interval' => $userProduct->trial_interval,
				'sell_price' => $userProduct->sell_price,
				'billing_period' => $userProduct->billing_period,
				'billing_interval' => $userProduct->billing_interval,
				'billing_cycles' => $userProduct->billing_cycles,
				'tax' => $userProduct->tax,
				'shipping_fees' => $userProduct->shipping_fees,
				'quantity' => $userProduct->quantity,
				'coupon_id' => $userProduct->coupon_id,
				'invoice_id' => $userProduct->invoice_id,
			), 
			array( 
				'%d',
				'%d',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
			)
		);
		$result = $this->wpdb->insert_id;
		//echo $this->wpdb->last_query;
		return $result;
	}
}
?>
