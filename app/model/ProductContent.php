<?php
class ProductContent extends Model{

	public function findAllPageIds() {

		$sql     = "SELECT page_id FROM memba_product_content GROUP BY page_id";
		$rows = $this->wpdb->get_results($sql);

		$pageIds = array();
		foreach($rows as $row){
        	$pageIds[] = $row->page_id;
        }

        return $pageIds;
	}

	public function findUserAccessiblePageIds($user){

		$userId = $user->id;
		$timezone = $user->timezone;
		$date = new DateTime('now', new DateTimeZone($timezone) );
		$strDate = $date->format('Y-m-d');

		$sql   = "SELECT date_start, page_id, days";
	    $sql  .= " FROM memba_user_product, memba_product_content";
	    $sql  .= " WHERE memba_user_product.product_id = memba_product_content.product_id";
	    $sql  .= " AND memba_user_product.user_id = $userId";
	    $sql  .= " AND memba_user_product.is_cancel = 0";
	    $sql  .= " AND (memba_user_product.date_end >= NOW() OR memba_user_product.date_end IS NULL)";
	    $sql  .= " GROUP BY page_id";
	    $rows = $this->wpdb->get_results($sql);

	    $pageIds = array();
	    foreach($rows as $row){
	    	$days = $row->days;
			$pageShowDate = strtotime($row->date_start. " + $days days");

			if($pageShowDate <= strtotime($strDate)) {
				$pageIds[] = $row->page_id;
			}
	    }

	    return $pageIds;

	}

	public function findUserProductAndContent($user) {

		$timezone = $user->timezone;
		$date = new DateTime('now', new DateTimeZone($timezone) );
		$strDate = $date->format('Y-m-d');

		$userProductModel = new UserProduct();
		$products = $userProductModel->findUserProducts($user);
		
		foreach($products as $product){
			
			$product->pages = array();

			$sql = "SELECT page_id, days";
			$sql .= " FROM memba_product_content";
			$sql .= " WHERE product_id = $product->id ORDER BY days DESC";
	
			$pages = $this->wpdb->get_results($sql);
			foreach($pages as $page)
			{
				$days = $page->days;
				$pageShowDate = strtotime($product->date_start. " + $days days");

				if($pageShowDate <= strtotime($strDate)) {
					$product->pages[] = $page;
				}
			}
		}

		$productsWithPages = array();

		foreach($products as $product){
			if(count($product->pages) > 0) {
				$productsWithPages[] = $product;
			}
		}

		return $productsWithPages;
	}

}
?>