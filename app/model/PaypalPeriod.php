<?php
class PaypalPeriod extends Model{

public function determinatePeriod($p){
	switch ($p) {
		case 'D':
			return __('day(s)', THIS_PLUGIN_NAME);
			break;
		case 'W':
			return __('week(s)', THIS_PLUGIN_NAME);
			break;
		case 'M':
			return __('month(s)', THIS_PLUGIN_NAME);
			break;
		case 'Y':
			return __('year(s)', THIS_PLUGIN_NAME);
			break;
	}
}

public function determinateNvpPeriod($p){
	switch ($p) {
		case 'D':
			return 'Day';
			break;
		case 'W':
			return 'Week';
			break;
		case 'M':
			return 'Month';
			break;
		case 'Y':
			return 'Year';
			break;
	}
}

public function determineCycleLabel($p, $i ,$c){

	$label = '';
	if($i == 1){
		switch ($p) {
			case 'D':
				if($c == 30){
					$label = __('during 1 month', THIS_PLUGIN_NAME);
				}
				break;
			case 'W':
				$label = __('during 1 month', THIS_PLUGIN_NAME);
				break;
			case 'M':
				if($c == 12){
					$label = __('during 2 month', THIS_PLUGIN_NAME);
				}
				break;
			case 'Y':
				if($c == 12){
					$label = __('during 2 month', THIS_PLUGIN_NAME);
				}
				break;
		}
	}

}

public function findAll(){
	$records = array();
	foreach(self::$list as $pairs){

		$pair = explode(":", $pairs);
		$obj = new StdClass();
		$obj->code = $pairs;
		$obj->p = $pair[0];
		$obj->t = intval($pair[1]);
		
		if($obj->t == 0){
			$obj->name = '-';

		} else {
			$period = $this->determinatePeriod($obj->p);
			
			$obj->name .= $obj->t." ";
			
			$obj->name .= $period;
		}
		$records[] = $obj;
	}
	return $records;
}

public static $list = array(
'D:1',
'D:2',
'D:3',
'D:4',
'D:5',
'D:6',
'D:7',
'D:8',
'D:9',
'D:10',
'D:11',
'D:12',
'D:13',
'D:14',
'D:15',
'D:16',
'D:17',
'D:18',
'D:19',
'D:20',
'D:21',
'D:22',
'D:23',
'D:24',
'D:25',
'D:26',
'D:27',
'D:28',
'D:29',
// 'D:30',
// 'D:31',
// 'D:32',
// 'D:33',
// 'D:34',
// 'D:35',
// 'D:36',
// 'D:37',
// 'D:38',
// 'D:39',
// 'D:40',
// 'D:41',
// 'D:42',
// 'D:43',
// 'D:44',
// 'D:45',
// 'D:46',
// 'D:47',
// 'D:48',
// 'D:49',
// 'D:50',
// 'D:51',
// 'D:52',
// 'D:53',
// 'D:54',
// 'D:55',
// 'D:56',
// 'D:57',
// 'D:58',
// 'D:59',
// 'D:60',
// 'D:61',
// 'D:62',
// 'D:63',
// 'D:64',
// 'D:65',
// 'D:66',
// 'D:67',
// 'D:68',
// 'D:69',
// 'D:70',
// 'D:71',
// 'D:72',
// 'D:73',
// 'D:74',
// 'D:75',
// 'D:76',
// 'D:77',
// 'D:78',
// 'D:79',
// 'D:80',
// 'D:81',
// 'D:82',
// 'D:83',
// 'D:84',
// 'D:85',
// 'D:86',
// 'D:87',
// 'D:88',
// 'D:89',
// 'D:90',
'W:1',
'W:2',
'W:3',
// 'W:4',
// 'W:5',
// 'W:6',
// 'W:7',
// 'W:8',
// 'W:9',
// 'W:10',
// 'W:11',
// 'W:12',
// 'W:13',
// 'W:14',
// 'W:15',
// 'W:16',
// 'W:17',
// 'W:18',
// 'W:19',
// 'W:20',
// 'W:21',
// 'W:22',
// 'W:23',
// 'W:24',
// 'W:25',
// 'W:26',
// 'W:27',
// 'W:28',
// 'W:29',
// 'W:30',
// 'W:31',
// 'W:32',
// 'W:33',
// 'W:34',
// 'W:35',
// 'W:36',
// 'W:37',
// 'W:38',
// 'W:39',
// 'W:40',
// 'W:41',
// 'W:42',
// 'W:43',
// 'W:44',
// 'W:45',
// 'W:46',
// 'W:47',
// 'W:48',
// 'W:49',
// 'W:50',
// 'W:51',
// 'W:52',
'M:1',
'M:2',
'M:3',
'M:4',
'M:5',
'M:6',
'M:7',
'M:8',
'M:9',
'M:10',
'M:11',
// 'M:12',
// 'M:13',
// 'M:14',
// 'M:15',
// 'M:16',
// 'M:17',
// 'M:18',
// 'M:19',
// 'M:20',
// 'M:21',
// 'M:22',
// 'M:23',
// 'M:24',
'Y:1',
// 'Y:2',
// 'Y:3',
// 'Y:4',
// 'Y:5',
);
}
?>