<?php
class Product extends Model{

	const PRODUCT_BUTTON_SHORTCODE = 'product_button';

	public static $types = array(
		1 => 'One time payment (lifetime access)',
		2 => 'One time payment (limited access period)',
		3 => 'Subscription',
		4 => 'Subscription (with trial period)',
		5 => 'Term subscription',
		6 => 'Term subscription (with trial period)',
	);

	public function getBillingAgreement($product){

		$paypalPeriodModel = new PaypalPeriod();
		$currencyModel = new Currency();

		$currency = $currencyModel->findByCode($product->currency);
		$symbol = $currency->symbol;

		$sell_price = floatval($product->sell_price).$symbol ;
		$trial_price = floatval($product->trial_price).$symbol ;
		if($trial_price == 0){
			$trial_price = __('Free');
		} 
		$trial_interval = $product->trial_interval;
		if(intval($product->trial_interval) == 1){
			$trial_interval = '';
		}

		$billing_interval = intval($product->billing_interval);
		$billing_cycles = $product->billing_cycles;
		$trial_period = $paypalPeriodModel->determinatePeriod($product->trial_period);
		$billing_period = $paypalPeriodModel->determinatePeriod($product->billing_period);
		
		switch ($product->type) {
			case 1:
				return $sell_price.' '.__('for a life time access').'.';
				break;
			case 2:
				return $sell_price.' '.__('for').' '.$billing_interval.' '.$billing_period.'.';
				break;
			case 3:
				if($billing_interval == 1){
					$billing_interval = '';
				}
				return $sell_price.' '.__('every').' '.$billing_interval.' '.$billing_period.'.';
				break;
			case 4:
			    
				$str = $trial_price.' '.__('for the first').' '.$trial_interval.' '.$trial_period;
				$str .= __('then').', '.$sell_price.' '.__('every').' '.$billing_interval.' '.$billing_period.'.';
				return $str;
				break;
			case 5:
			    if($billing_interval == 1){
					$billing_interval = '';
				}
				$str = $sell_price.' '.__('every').' '.$billing_interval.' '.$billing_period;
				$str .= __('for').' '. $billing_cycles.' '.__('cycles').'.';
				return $str;
				break;
			case 6:
				if($billing_interval == 1){
					$billing_interval = '';
				}
				$str = $trial_price.' '.__('for the first').' '.$trial_interval.' '.$trial_period;
				$str .= __('then').', '.$sell_price.' '.__('every').' '.$billing_interval.' '.$billing_period;
				$str .= __('for').' '.$billing_cycles.' '.__('cycles').'.';
				return $str;
				break;
		}
	}

	public function findById($id) {
		$sql = "SELECT * FROM ".THIS_PLUGIN_NAME."_product WHERE id=$id ";
		$row = $this->wpdb->get_row($sql);
		if($row){
			$row->type = intval($row->type);
		}
		return $row;
	}

	public function findAll($items_per_page = null, $page = null, $current_view = null){

		$total = $this->wpdb->get_var( "SELECT count(id) FROM ".THIS_PLUGIN_NAME."_product" );
		$total = intval($total);

	    $items_per_page = isset( $items_per_page ) ? $items_per_page  : 1;
	    $page = isset( $page ) ? $page  : 1;
	    $offset = ( $page * $items_per_page ) - $items_per_page;

	    $sql = "SELECT * FROM ".THIS_PLUGIN_NAME."_product";
	    $sql .= " LIMIT $offset, $items_per_page";

		$rows = $this->wpdb->get_results(  $sql );
	
		$pagination = paginate_links( array(
		    'base' => add_query_arg( array('paged' => '%#%', 'page' => $current_view ) ),
		    'format' => '',
		    'prev_text' => __('&laquo;'),
		    'next_text' => __('&raquo;'),
		    'total' => ceil($total / $items_per_page),
		    'current' => $page
		));


		return array( $rows, $pagination );
	}

	public function valid($product){
		$response = array('result' => 1, 'OK');
		return $response;

	}

	public function save($product){

		if(isset($product->id)){
			$result = $this->wpdb->update( 
				THIS_PLUGIN_NAME.'_product', 
				array( 
					'name' => $product->name,
					'type' => $product->type,
					'currency' => $product->currency,
					'sell_price' => $product->sell_price,
					//'bought_price' => $product->bought_price,
					'stock' => $product->stock,
					'billing_interval' => $product->billing_interval,
					'billing_period' => $product->billing_period,
					'billing_cycles' => $product->billing_cycles,
					'tax' => $product->tax,
					'shipping_fees' => $product->shipping_fees,
					'trial_price' => $product->trial_price,
					'trial_interval' => $product->trial_interval,
					'trial_period' => $product->trial_period,
				), 
				array( 'id' => $product->id ), 
				array( 
					'%s', //name
					'%s', //type
					'%s', //currency
					'%s', //sell_price
					//'%s', //bought_price
					'%s', //stock
					'%s', //billing_interval
					'%s', //billing_period
					'%s', //billing_cycles
					'%s', //tax
					'%s', //shipping_fees
					'%s', //trial_price
					'%s', //trial_interval
					'%s', //trial_period
				), 
				array( '%d' ) 
			);
		} else {
			$result = $this->wpdb->insert( 
				THIS_PLUGIN_NAME.'_product', 
				array( 
					'name' => $product->name,
					'type' => $product->type,
					'currency' => $product->currency,
					'sell_price' => $product->sell_price,
					//'bought_price' => $product->bought_price,
					'stock' => $product->stock,
					'billing_interval' => $product->billing_interval,
					'billing_period' => $product->billing_period,
					'billing_cycles' => $product->billing_cycles,
					'currency' => $product->currency,
					'tax' => $product->tax,
					'shipping_fees' => $product->shipping_fees,
					'trial_price' => $product->trial_price,
					'trial_interval' => $product->trial_interval,
					'trial_period' => $product->trial_period,
				), 
				array( 
					'%s', //name
					'%s', //type
					'%s', //currency
					'%s', //sell_price
					//'%s', //bought_price
					'%s', //stock
					'%s', //billing_interval
					'%s', //billing_period
					'%s', //billing_cycles
					'%s', //tax
					'%s', //shipping_fees
					'%s', //trial_price
					'%s', //trial_interval
					'%s', //trial_period
				)
			);
		}
		//echo $this->wpdb->last_query;
	}
}
?>