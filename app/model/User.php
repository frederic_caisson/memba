<?php
class User extends Model{

	public function findAll($items_per_page = null, $page = null, $current_view = null){

		$total = $this->wpdb->get_var( "SELECT count(id) FROM ".THIS_PLUGIN_NAME."_user" );
		$total = intval($total);

	    $items_per_page = isset( $items_per_page ) ? $items_per_page  : 1;
	    $page = isset( $page ) ? $page  : 1;
	    $offset = ( $page * $items_per_page ) - $items_per_page;

	    $sql = "SELECT * FROM ".THIS_PLUGIN_NAME."_user";
	    $sql .= " LIMIT $offset, $items_per_page";

		$rows = $this->wpdb->get_results(  $sql );
	
		$pagination = paginate_links( array(
		    'base' => add_query_arg( array('paged' => '%#%', 'page' => $current_view ) ),
		    'format' => '',
		    'prev_text' => __('&laquo;'),
		    'next_text' => __('&raquo;'),
		    'total' => ceil($total / $items_per_page),
		    'current' => $page
		));


		return array( $rows, $pagination );
	}

	public function findByEmail($email) {
		$sql = "SELECT * FROM ".THIS_PLUGIN_NAME."_user WHERE email='$email' ";
		$row = $this->wpdb->get_row($sql);
		if($row){
			$row->password = Util::decrypt($row->password);
		}
		return $row;
	}

	public function findByPaypalEmail($paypal_email) {
		$sql = "SELECT * FROM ".THIS_PLUGIN_NAME."_user WHERE paypal_email='$paypal_email' ";
		$row = $this->wpdb->get_row($sql);
		return $row;
	}

	public function findById($id) {
		$sql = "SELECT * FROM ".THIS_PLUGIN_NAME."_user WHERE id=$id ";
		$row = $this->wpdb->get_row($sql);
		if($row){
		$row->password = Util::decrypt($row->password);
		}
		return $row;
	}

	public function valid($user) {

		$response = array('result' => 1, 'OK');

		if(isset($user->confirm)) {

			if($user->password != $user->confirm) {
				$response = array('result' => 0, 'message'=> __('Passwords are not matching',THIS_PLUGIN_NAME));
				return $response;
			}
		}

		$existUser = $this->findByEmail($user->email);
		if($existUser) {
			if($user->id != $existUser->id) {
				$response = array('result' => 0, 'message'=> __('This email is already used by another user',THIS_PLUGIN_NAME));
			    return $response;
			}
		}
		
		return $response;

	}

	public function save($user) {

		if(empty($user->postal)) {
			$user->postal = self::NULL;
		}
		if(empty($user->address1)) {
			$user->address1 = self::NULL;
		}
		if(empty($user->address2)) {
			$user->address2 = self::NULL;
		}
		if(empty($user->tel)) {
			$user->tel = self::NULL;
		}

		$password = Util::encrypt($user->password);

		$result = false;
		if(!empty($user->id)){
			$result = $this->wpdb->update( 
				THIS_PLUGIN_NAME.'_user', 
				array( 
					'email' => $user->email,
					'paypal_email' => $user->paypal_email,
					'first_name' => $user->first_name,
					'last_name' => $user->last_name,
					'postal' => $user->postal,
					'address1' => $user->address1,
					'address2' => $user->address2,
					'tel' => $user->tel,
					'password' => $password,
					'timezone' => $user->timezone,
				), 
				array( 'id' => $user->id ), 
				array( 
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
				), 
				array( '%d' ) 
			);
		} else {
			$result = $this->wpdb->insert( 
				THIS_PLUGIN_NAME.'_user', 
				array( 
					'email' => $user->email,
					'paypal_email' => $user->paypal_email,
					'first_name' => $user->first_name,
					'last_name' => $user->last_name,
					'postal' => $user->postal,
					'address1' => $user->address1,
					'address2' => $user->address2,
					'tel' => $user->tel,
					'password' => $password,
					'timezone' => $user->timezone,
				), 
				array( 
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
				)
			);
			$result = $this->wpdb->insert_id;
		}
		//echo $this->wpdb->last_query;
		return $result;
	}

	function delete($user){
		return $this->wpdb->delete( THIS_PLUGIN_NAME.'_user', array( 'id' => $user->id ) );
	}
}

?>