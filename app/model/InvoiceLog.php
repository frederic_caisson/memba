<?php
class InvoiceLog extends Model{

	const STATUS_ERROR = -1;
	const STATUS_VALIDATED = 1;
	const STATUS_RECCURENT = 2;

	public static function generateInvoiceId($product){
		$uid = uniqid();
		$uid = strtoupper($uid);
		return $uid.'-'.$product->type.'-'.$product->id;
	}

	public function log($invoiceLog){
		$this->wpdb->insert( 
				THIS_PLUGIN_NAME.'_invoice_log',
				array( 
					'invoice_id' =>	$invoiceLog->invoice_id,
					'date' => date('Y-m-d H:i:s'),
					'status' => $invoiceLog->status,
					'log' => $invoiceLog->log,
					'data' => $invoiceLog->data,
				),
				array( 
					'%s', 
					'%s',
					'%s', 
					'%s', 
					'%s', 
				)
			);
	}
}
?>