<?php
class Config extends Model{
	const PAYMENT_PAYPAL_KEY = 'paypal';
	
	function findByKey($key){
		$sql = "SELECT * FROM ".THIS_PLUGIN_NAME."_config WHERE `key` = '$key'";

		$result = $this->wpdb->get_row($sql);

		if($result) {
			if(!empty($result->config)){
				return json_decode($result->config);
			};
		} 
		return null;
	}

	function save($key, $config){
		if($this->findByKey($key)){
			$result = $this->wpdb->update( 
				THIS_PLUGIN_NAME.'_config', 
				array( 
					'config' => $config,
				), 
				array( 'key' => $key ), 
				array( 
					'%s',
				), 
				array( '%s' ) 
			);
		} else {
			$result = $this->wpdb->insert(
				THIS_PLUGIN_NAME.'_config', 
				array( 
					'key' => $key,
					'config' => $config,
				), 
				array( 
					'%s',
					'%s',
				)
			);
		}
	}
}
?>