<script>
	var sell_price_label = '<?php echo __('I will charge');?>';
	var sell_price_trial_label = '<?php echo __('Then, I will charge');?>';
	var sell_price_every_label = '<?php echo __('every');?>';
	var sell_price_one_label = '<?php echo __('I will charge one time');?>';
	var sell_price_limited_label = '<?php echo __('for a limited access of');?>';
</script>


<h2><?php echo __('Create Product', THIS_PLUGIN_NAME); ?><h2>

<?php 
if(isset($response)): 

	if($response['result'] == 1){
		$class = 'updated';
	} else {
		$class = 'error';
	}
?>

<div class="<?php echo $class ?>"><p><?php echo $response['message'] ?></p></div>
<?php endif;?>

<?php if($class != 'updated'): ?>
<form id="form_product_create" method="post" action="">
	<table class="form-table">
		<tr>
			<th scope="row"><label for="name"><?php echo __('Name',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="name" name="name" type="text" value="<?php echo $product->name; ?>" maxlength="255" required/></td>
		</tr>
		<!-- currency -->
		<tr>
			<th scope="row"><label for="currency"><?php echo __('Currency',THIS_PLUGIN_NAME); ?></label></th>
			<td><select name="currency">
			<?php foreach ($currencies as $currency): ?>
				<?php 
				$selected = '';
				if($product->currency == $currency->code){
					$selected = 'selected="selected"';
				}
				?>
				<option value="<?php echo $currency->code ?>" <?php echo $selected?>><?php echo $currency->name ?></option>
			<?php endforeach; ?>
			</select></td>
		</tr>
		<!-- type -->
		<tr>
			<th scope="row"><label for="type"><?php echo __('Payment type',THIS_PLUGIN_NAME); ?></label></th>
			<td><select name="type" id="type">
			<?php foreach ($types as $type => $type_desc): ?>
				<?php 
				$selected = '';
				if($product->type == $type){
					$selected = 'selected="selected"';
				}
				?>
				<option value="<?php echo $type ?>" <?php echo $selected?>><?php echo __($type_desc,THIS_PLUGIN_NAME); ?></option>
			<?php endforeach; ?>
			</select></td>
		</tr>
		<!-- trial -->
		<tr class="trial-fields">
			<th scope="row" ><label for="trial_price"><?php echo __('I will offer the first',THIS_PLUGIN_NAME); ?></label></th>
			<td>
				<select name="trial_period">
					<?php 
					foreach ($paypal_periods as $period):
						$selected = '';
					    $product_period = $product->trial_period.':'.$product->trial_interval;
						if($product_period == $period){
							$selected = 'selected="selected"';
						}
						?>
						<option value="<?php echo $period->code ?>" <?php echo $selected?>><?php echo $period->name ?></option>
					<?php endforeach; ?>
				</select> 
				at 
				<input id="trial_price" name="trial_price" type="text" value="<?php echo $product->trial_price; ?>" maxlength="255" onkeypress="return isFloat(this.value, event)"/>
			</td>
		</tr>
		<!-- sell -->
		<tr>
			<th scope="row"><label for="sell_price" id="sell_price_label"></label></th>
			<td>
				<input id="sell_price" name="sell_price" type="text" width="100px" value="<?php echo $product->sell_price; ?>" maxlength="255" onkeypress="return isFloat(this.value, event)" required/>
				<span class="sell-period" id="sell_price_label2"></span>
				<select name="sell_period" class="sell-period">
				<?php 
				foreach ($paypal_periods as $period):
					$selected = '';
					$product_period = $product->billing_period.':'.$product->billing_interval;
					if($product_period == $period){
						$selected = 'selected="selected"';
					}
					?>
					<option value="<?php echo $period->code ?>" <?php echo $selected?>>
						
						<?php 
						echo $period->name 
						?>
					</option>
				<?php endforeach; ?>
				</select> 
			</td>
		</tr>
		<tr class="cycle-fields">
			<th scope="row"></th>
			<td><input id="billing_cycles" name="billing_cycles" type="number" value="<?php echo $product->billing_cycles; ?>" maxlength="255" min="0" max="999" onkeypress="return isNumber(event)" /> <?php echo __('time(s)',THIS_PLUGIN_NAME); ?></td>
		</tr>

		<tr>
			<th scope="row"><label for="stock"><?php echo __('Stock',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="stock" name="stock" type="text" value="<?php echo $product->stock; ?>" maxlength="255" /></td>
		</tr>
		<tr>
			<th scope="row"><label for="tax"><?php echo __('Tax',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="tax" name="tax" type="text" value="<?php echo $product->tax; ?>" maxlength="255" onkeypress="return isFloat(this.value, event)" /></td>
		</tr>
		<tr>
			<th scope="row"><label for="shipping_fees"><?php echo __('Shipping fees',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="shipping_fees" name="shipping_fees" type="text" value="<?php echo $product->shipping_fees; ?>" maxlength="255" onkeypress="return isFloat(this.value, event)" /></td>
		</tr>
		
	</table>
	<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php echo __('Save Changes');?>"  /></p>
</form>
<?php else: ?>
	<a href="admin.php?page=<?php echo $menu_product_slug ?>" ><?php echo __('Return to products list page',THIS_PLUGIN_NAME); ?></a>
<?php endif;?>