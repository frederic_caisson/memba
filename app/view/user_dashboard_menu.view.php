<?php $selected_class = "class='m-navigation-selected'"; 

?>
<div class="m-navigation">
    <ul>
        <li <?php if($current_view == 'user_dashboard') echo $selected_class; ?>><a href="<?php echo $user_dashboard_page; ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home',THIS_PLUGIN_NAME) ?></a></li>
        <li <?php if($current_view == 'user_dashboard_products') echo $selected_class; ?>><a href="<?php echo $user_dashboard_page.'?view=user_dashboard_products'; ?>"><i class="fa fa-cube"></i> <?php echo __('My Products',THIS_PLUGIN_NAME) ?></a></li>
        <li <?php if($current_view == 'user_dashboard_invoices') echo $selected_class; ?>><a href="<?php echo $user_dashboard_page.'?view=user_dashboard_invoices'; ?>"><i class="fa fa-ticket"></i> <?php echo __('My Invoices',THIS_PLUGIN_NAME) ?></a></li>
        <li <?php if($current_view == 'user_dashboard_settings') echo $selected_class; ?>><a href="<?php echo $user_dashboard_page.'?view=user_dashboard_settings'; ?>"><i class="fa fa-wrench"></i> <?php echo __('Settings',THIS_PLUGIN_NAME) ?></a></li>
        <li><a href="<?php echo $user_dashboard_page.'?view=user_logout'; ?>"><i class="fa fa-sign-out"></i> <?php echo __('Logout',THIS_PLUGIN_NAME) ?></a></li>
    </ul>
</div>