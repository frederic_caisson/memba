<h2><?php echo __('Pages settings', THIS_PLUGIN_NAME); ?><h2>

<?php 
if($pageSettings['result'] == 0): 
	foreach($pageSettings['messages'] as $message):
?>
	<div class="error"><p><?php echo $message ?></p></div>
<?php 
	endforeach;
endif; 
?>

<?php 
if(isset($response)): 

	if($response['result'] == 1){
		$class = 'updated';
	} else {
		$class = 'error';
	}
?>
<div class="<?php echo $class ?>"><p><?php echo $response['message'] ?></p></div>
<?php endif;?>

<table class="form-table">
<?php foreach($items as $key => $item): ?>
<tr>
		<th scope="row"><label for="<?php echo  $key ?>"><?php echo $labels[$key] ?></label></th>
		<td>
			<?php if(!empty($items[$key])): ?>
				<a target="_blank" href="post.php?post=<?php echo $items[$key]; ?>&action=edit" class="button button-secondary"><?php echo __("Edit page", THIS_PLUGIN_NAME); ?></a>
				&nbsp;
				<a target="_blank" href="<?php echo site_url().'?p='.$items[$key]; ?>" class="button button-secondary"><?php echo __("View page", THIS_PLUGIN_NAME); ?></a>

				<input type="submit" name="delete_<?php echo $key ?>" id="delete_<?php echo $key ?>" class="button button-secondary submitdelete" style="color:red" value="<?php echo __('Delete', THIS_PLUGIN_NAME);?>" data-slug="<?php echo $key ?>" data-id="<?php echo $items[$key]; ?>" />
			<?php else: ?>
				<i class="fa fa-chain-broken"></i> <b style="color:grey"><?php echo __('No linked page (Create a page)', THIS_PLUGIN_NAME); ?></b>
				&nbsp;
				<form id="form_create_<?php echo $key ?>" name="form_create_<?php echo $key ?>" method="post" action="" data-slug="<?php echo $key ?>">
					<input type="submit" name="create_<?php echo $key ?>" id="create_<?php echo $key ?>" class="button button-primary submitcreate" value="<?php echo __('Create', THIS_PLUGIN_NAME);?>" />
					<input type="text" name="page_name_<?php echo $key ?>" id="page_name_<?php echo $key ?>" placeholder="<?php echo __('Page name', THIS_PLUGIN_NAME) ?>" max_length="80" required />
				</form>
			<?php endif; ?>
		</td>
</tr>
<?php endforeach;?>
</table>
