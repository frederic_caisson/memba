<div class="wrap">
<h2><?php echo __('Members', THIS_PLUGIN_NAME); ?> <a href="admin.php?page=<?php echo $menu_member_slug ?>&create=1" class="add-new-h2"><?php echo __('Add New Member', THIS_PLUGIN_NAME); ?></a></h2>

<div id="message"></div>

<?php echo $pagination; ?>
<table class="widefat tablesorter">
	<thead>
		<tr>
			<th><a href='#'><?php echo __('ID', THIS_PLUGIN_NAME); ?></a></th>
			<th><a href='#'><?php echo __('Last Name', THIS_PLUGIN_NAME); ?></a></th>
			<th><a href='#'><?php echo __('First Name', THIS_PLUGIN_NAME); ?></a></th>
			<th><a href='#'><?php echo __('Email', THIS_PLUGIN_NAME); ?></a></th>        
			<th><a href='#'><?php echo __('Tel', THIS_PLUGIN_NAME); ?></a></th>
			<th><a href='#'><?php echo __('Address', THIS_PLUGIN_NAME); ?></a></th>
			<th><a href='#'><?php echo __('Timezone', THIS_PLUGIN_NAME); ?></a></th>
		</tr>
	</thead>
	<tbody>

		<?php foreach($rows as $row): ?>
			<tr class="row<?php echo $row->id ?>">
				<td><?php echo $row->id ?> <div class="row-actions"><span class="edit"><a href="admin.php?page=<?php echo $menu_member_slug ?>&edit=<?php echo $row->id ?>"><?php echo __('Edit', THIS_PLUGIN_NAME); ?></a> | </span><span class="trash"><a class="submitdelete" href="admin.php?page=<?php echo $menu_member_slug ?>&delete=" value="<?php echo $row->id ?>" ><?php echo __('Delete', THIS_PLUGIN_NAME); ?></a>
					</div></td>
				<td><?php echo $row->last_name ?></td>
				<td><?php echo $row->first_name ?></td>
				<td><?php echo $row->email ?></td>
				<td><?php echo $row->tel ?></td>
				<td><?php echo $row->address1 ?> <?php echo $row->address2 ?></td>
				<td><?php echo $row->timezone ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>

</table>
<?php echo $pagination; ?>

</div>