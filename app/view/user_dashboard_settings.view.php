<form class="m-form-settings" id="form_settings" action="" method="POST">
	<fieldset>
			<div class="m-input-group">
				<label for="email"><?php echo __('Email',THIS_PLUGIN_NAME); ?></label>
	            <input id="email" name="email" type="email" value="<?php echo $user->email; ?>" maxlength="255" required/>
			</div>
		
			<div class="m-input-group">
				<label for="first_name"><?php echo __('First name',THIS_PLUGIN_NAME); ?></label>
	            <input id="first_name" name="first_name" type="text" value="<?php echo $user->first_name; ?>" maxlength="255" required/>
			</div>
		
			<div class="m-input-group">
				<label for="last_name"><?php echo __('Last Name',THIS_PLUGIN_NAME); ?></label>
	            <input id="last_name" name="last_name" type="text" value="<?php echo $user->last_name; ?>" maxlength="255" required/>
            </div>
		
			<div class="m-input-group">
				<label for="postal"><?php echo __('Postal',THIS_PLUGIN_NAME); ?></label>
            	<input id="postal" name="postal" type="text" value="<?php echo $user->postal; ?>" maxlength="20" onkeypress="return isNumberDash(event)" />
			</div>

			<div class="m-input-group">
				<label for="address1"><?php echo __('Adress 1',THIS_PLUGIN_NAME); ?></label>
            	<input class="pure-input-1" id="address1" name="address1" value="<?php echo $user->address1; ?>" maxlength="255" type="text"/>
			</div>

			<div class="m-input-group">
				<label for="address2"><?php echo __('Adress 2',THIS_PLUGIN_NAME); ?></label>
            	<input class="pure-input-1" id="address2" name="address2" value="<?php echo $user->address2; ?>" maxlength="255" type="text"/>
			</div>

			<div class="m-input-group">
				<label for="tel"><?php echo __('Phone',THIS_PLUGIN_NAME); ?></label>
            	<input id="tel" name="tel" type="text" value="<?php echo $user->tel; ?>" onkeypress="return isNumberDash(event)" maxlength="50" />
            </div>

            <div class="m-input-group">
				<label for="password"><?php echo __('Password',THIS_PLUGIN_NAME); ?></label>
				<input id="password" name="password" type="password" value="<?php echo $user->password; ?>" maxlength="128" required/>
			</div>

			<div class="m-input-group">
				<label for="confirm"><?php echo __('Confirm password',THIS_PLUGIN_NAME); ?></label>
				<input id="confirm" name="confirm" type="password" value="<?php echo $user->password; ?>" maxlength="128" required/>
			</div>
			<div class="m-input-group">
				<button class="m-submit" type="submit" ><?php echo __('Save',THIS_PLUGIN_NAME); ?></button>
			</div>
	</fieldset>
</form>
<div id="message" class="m-message" style="display:none"></div>