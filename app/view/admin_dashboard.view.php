<div class="wrap">
<h2><?php _e('Dashboard', THIS_PLUGIN_NAME); ?> <a href="#" class="add-new-h2"><?php _e('Add New Member', THIS_PLUGIN_NAME); ?></a></h2>

<table class="widefat">
	<thead>
		<tr>
			<th><?php _e('ID', THIS_PLUGIN_NAME); ?></th>
			<th><?php _e('Last Name', THIS_PLUGIN_NAME); ?></th>
			<th><?php _e('First Name', THIS_PLUGIN_NAME); ?></th>
			<th><?php _e('Email', THIS_PLUGIN_NAME); ?></th>        
			<th><?php _e('Tel', THIS_PLUGIN_NAME); ?></th>
			<th><?php _e('Address', THIS_PLUGIN_NAME); ?></th>
			<th><?php _e('Timezone', THIS_PLUGIN_NAME); ?></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</thead>
</table>
</div>