<?php get_header(); ?>
<div class="m-checkout-canvas">
<div class="m-checkout-page">
	<h2><?php echo $product->name ?></h2>

	<i><?php echo $agreement ?></i>

	<hr>
	<div id="message" class="m-message m-message-error" style="display:none"></div>
	
	<form id="form_checkout" name="form_checkout" action="<?php echo $url ?>" method="POST">

		<input type="hidden" name="product_id" id="product_id" value="<?php echo $product->id ?>"/>

		<input type="hidden" name="timezone" id="timezone" value=""/>
		
		<?php if($user): ?>
			<h4><?php echo __('Member informations', THIS_PLUGIN_NAME) ?></h4>
			<div class="m-checkout-panel">
				<div class="m-input-group">
					<label><?php echo __('Last name', THIS_PLUGIN_NAME) ?>: <?php echo $user->last_name ?></label>
				</div>
				<div class="m-input-group">
					<label><?php echo __('First name', THIS_PLUGIN_NAME) ?>: <?php echo $user->first_name ?></label>
				</div>
				<div class="m-input-group">
					<label><?php echo __('Paypal account', THIS_PLUGIN_NAME) ?>: <?php echo $user->paypal_email?></label>
				</div>
			</div>
		<?php else: ?>
			<div class="m-checkout-panel">
				<h4><?php echo __('You are already a member', THIS_PLUGIN_NAME) ?></h4>
				<div class="m-input-group">
					<label for="email"><?php echo __('Member Email', THIS_PLUGIN_NAME); ?></label> 
		            <input id="email" name="email" type="email" maxlength="255" placeholder="<?php echo __('your@email.com', THIS_PLUGIN_NAME); ?>" />
				</div>
				<div class="m-input-group">
					<label for="password"><?php echo __('Member Password', THIS_PLUGIN_NAME); ?></label> 
					<input id="password" name="password" type="password" maxlength="128" placeholder="<?php echo __('your secret password', THIS_PLUGIN_NAME); ?>" />
				</div>
			</div>
			<div class="m-checkout-panel">
			&nbsp;	<?php echo __('Or', THIS_PLUGIN_NAME) ?> &nbsp;
			</div>	
			<div class="m-checkout-panel">
				<h4><?php echo __('You are a new member', THIS_PLUGIN_NAME) ?></h4>
				<div class="m-input-group">
						<label for="paypal_email"><?php echo __('Paypal Email Account', THIS_PLUGIN_NAME); ?></label> 
			            <input id="paypal_email" name="paypal_email" type="email" maxlength="255" placeholder="<?php echo __('paypal@email.com', THIS_PLUGIN_NAME); ?>" />
				</div>
			</div>
		<?php endif; ?>
		
		<div class="m-clear"></div>
		<span style="text-align:center;">
			<input type="submit" id="submit" class="m-paypal-button" value="<?php echo __('Continue to PayPal', THIS_PLUGIN_NAME) ?>"/> 
		</span>
	</form>

</div>
</div>
<?php get_footer(); ?>
