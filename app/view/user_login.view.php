<div id="message" class="m-message m-message-error" style="display:none"></div>
<form class="m-login" id="form_login" action="" method="POST">
	<fieldset>
		<div class="m-input-group">
			<label for="email"><i class="fa fa-envelope"></i> <?php echo __('Email',THIS_PLUGIN_NAME); ?></label> 
            <input id="email" name="email" type="email" maxlength="255" placeholder="<?php echo __('your@email.com',THIS_PLUGIN_NAME); ?>" required/>
		</div>
		<div class="m-input-group">
			<label for="password"> <i class="fa fa-key"></i> <?php echo __('Password',THIS_PLUGIN_NAME); ?></label> 
			<input id="password" name="password" type="password" maxlength="128" placeholder="<?php echo __('your secret password',THIS_PLUGIN_NAME); ?>" required/>
		</div>
		
		<div class="m-input-group">
		<button class="m-submit" type="submit" ><i class="fa fa-sign-in"></i> <?php echo __('Login',THIS_PLUGIN_NAME); ?></button>
		</div>
	</fieldset>
</form>