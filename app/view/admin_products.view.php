<div class="wrap">
<h2><?php echo __('Products', THIS_PLUGIN_NAME); ?> <a href="admin.php?page=<?php echo $menu_product_slug ?>&create=1" class="add-new-h2"><?php echo __('Add New Product', THIS_PLUGIN_NAME); ?></a></h2>

<div id="message"></div>
<table class="widefat tablesorter">
	<thead>
		<tr>
			<th><a href='#'><?php echo __('ID', THIS_PLUGIN_NAME); ?></a></th>
			<th><a href='#'><?php echo __('Name', THIS_PLUGIN_NAME); ?></a></th>
			<th><a href='#'><?php echo __('Sell price', THIS_PLUGIN_NAME); ?></a></th>
			<th><a href='#'><?php echo __('Bought price', THIS_PLUGIN_NAME); ?></a></th>        
			<th><a href='#'><?php echo __('Is a subscription', THIS_PLUGIN_NAME); ?></a></th>
			<th><a href='#'><?php echo __('Reccuring payment days', THIS_PLUGIN_NAME); ?></a></th>
		</tr>
	</thead>
	<tbody>

		<?php foreach($rows as $row): ?>
			<tr class="row<?php echo $row->id ?>">
				<td><?php echo $row->id ?> <div class="row-actions"><span class="edit"><a href="admin.php?page=<?php echo $menu_product_slug ?>&edit=<?php echo $row->id ?>"><?php echo __('Edit', THIS_PLUGIN_NAME); ?></a> | </span><span class="trash"><a class="submitdelete" href="admin.php?page=<?php echo $menu_product_slug ?>&delete=" value="<?php echo $row->id ?>" ><?php echo __('Delete', THIS_PLUGIN_NAME); ?></a>
					</div></td>
				<td><?php echo $row->name ?></td>
				<td><?php echo $row->sell_price ?></td>
				<td><?php echo $row->bought_price ?></td>
				<td><?php if($row->is_recurrent){ echo __('yes', THIS_PLUGIN_NAME); }else{ echo __('no', THIS_PLUGIN_NAME); } ?></td>
				<td><?php echo $row->recurring_days ?></td>

			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php echo $pagination; ?>

</div>