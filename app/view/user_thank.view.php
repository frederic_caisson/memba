<?php get_header(); ?>
<div class="m-checkout-canvas">
	<div class="m-checkout-page">
		<h2><?php echo __('Thank you for your purchase!') ?></h2>

		<i><?php echo __('You will receive an email shortly with a link to your new product') ?></i>
	</div>
</div>
