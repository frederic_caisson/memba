<?php if(count($products) > 0): ?>
	<div class="m-bookshelf">
		<?php foreach($products as $product): ?>
			<div class="m-bookshelf-plain">
				<span class="m-badge"><?php echo $product->name; ?></span>
			</div>
			<div class="m-bookshelf-row">
				<div class="m-bookshelf-cell">
			  	<?php foreach($product->pages as $page): ?>
	  				<div class="m-product">
			  			<?php $page_id = $page->page_id; ?>
			  			<span class="m-product-name">
			  			<a href="<?php echo get_permalink( $page_id  );?>" ><?php echo get_the_title( $page_id  ); ?></a>
			  			<span><br>
			  			<?php 
			  			if(has_post_thumbnail( $page_id )):
			  				$product_img = get_the_post_thumbnail($page_id , 'full'); 
						else:
							$product_img = THIS_PLUGIN_BASE_DIR_HTTP.'/img/product-icon.png'; 
 						endif;
 						?>
			  			<a href="<?php echo get_permalink( $page_id  );?>" ><img class="m-product-image" src="<?php echo $product_img; ?>" width="70"/></a>
		  			</div>
			    <?php endforeach; ?>
			    </div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif ?>