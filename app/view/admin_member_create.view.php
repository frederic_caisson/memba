<h2><?php echo __('Create Member', THIS_PLUGIN_NAME); ?><h2>

<?php 
if(isset($response)): 

	if($response['result'] == 1){
		$class = 'updated';
	} else {
		$class = 'error';
	}
?>

<div class="<?php echo $class ?>"><p><?php echo $response['message'] ?></p></div>
<?php endif;?>

<?php if($class != 'updated'): ?>
<form id="form_member_create" method="post" action="">
	<table class="form-table">
		<tr>
			<th scope="row"><label for="email"><?php echo __('Email',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="email" name="email" type="email" value="<?php echo $user->email; ?>" maxlength="255" required/></td>
		</tr>
		<tr>
			<th scope="row"><label for="first_name"><?php echo __('First name',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="first_name" name="first_name" type="text" value="<?php echo $user->first_name; ?>" maxlength="255" required/></td>
		</tr>
		<tr>
			<th scope="row"><label for="last_name"><?php echo __('Last Name',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="last_name" name="last_name" type="text" value="<?php echo $user->last_name; ?>" maxlength="255" required/></td>
		</tr>
		<tr>
			<th scope="row"><label for="postal"><?php echo __('Postal',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="postal" name="postal" type="text" value="<?php echo $user->postal; ?>" maxlength="20" onkeypress="return isNumberDash(event)" /></td>
		</tr>
		<tr>
			<th scope="row"><label for="address1"><?php echo __('Adress 1',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="address1" name="address1" value="<?php echo $user->address1; ?>" maxlength="255" type="text"/></td>
		</tr>
		<tr>
			<th scope="row"><label for="address2"><?php echo __('Adress 2',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="address2" name="address2" value="<?php echo $user->address2; ?>" maxlength="255" type="text"/></td>
		</tr>
		<tr>
			<th scope="row"><label for="tel"><?php echo __('Tel',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="tel" name="tel" value="<?php echo $user->tel; ?>" maxlength="50" type="text" onkeypress="return isNumberDash(event)" /></td>
		</tr>
		<tr>
			<th scope="row"><label for="password"><?php echo __('Password',THIS_PLUGIN_NAME); ?></label></th>
			<td><input id="password" name="password" type="text" value="<?php echo $user->password; ?>" maxlength="128" required/></td>
		</tr>
		<tr>
			<th scope="row"><label for="timezone"><?php echo __('Timezone',THIS_PLUGIN_NAME); ?></label></th>
			<td><select name="timezone">
			<?php foreach ($timezones as $timezone): ?>
				<?php 
				$selected = '';
				if($user->timezone == $timezone['zone']){
					$selected = 'selected="selected"';
				}
				?>
				<option value="<? echo $timezone['zone'] ?>" <?php echo $selected?>><?php echo $timezone['zone'] ?></option>
			<?php endforeach; ?>
			</select></td>
		</tr>
	</table>
	<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php echo __('Save Changes');?>"  /></p>
</form>
<?php else: ?>
	<a href="admin.php?page=<?php echo $menu_member_slug ?>" ><?php echo __('Return to members list page',THIS_PLUGIN_NAME); ?></a>
<?php endif ?>