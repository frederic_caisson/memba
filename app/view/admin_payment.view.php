<h2><?php echo __('Payment settings', THIS_PLUGIN_NAME); ?><h2>

<form id="form_payment_settings" method="post" action="">
<table class="form-table">
	<tr>
		<th scope="row"></th>
		<td>
			<input type="radio" id="mode1" name="mode" value="1" <?php if($config->mode == 1) echo 'checked' ?>><?php echo __('Production mode', THIS_PLUGIN_NAME); ?>&nbsp;
			<input type="radio" id="mode0" name="mode" value="0" <?php if($config->mode == 0) echo 'checked' ?>><?php echo __('Test mode (will use sandbox email)', THIS_PLUGIN_NAME); ?>
		</td>
	</tr>

	<tr class="paypal-fields">
		<th scope="row"><label for="paypal_email"><?php echo __('Paypal email', THIS_PLUGIN_NAME); ?></label></th>
		<td><input type="email" name="paypal_email" value="<?php echo $config->paypal_email ?>"/></td>
	</tr>
	<tr class="paypal-fields">
		<th scope="row"><label for="paypal_api_username"><?php echo __('API Username', THIS_PLUGIN_NAME); ?></label></th>
		<td><input type="text" name="paypal_api_username" value="<?php echo $config->paypal_api_username ?>" /></td>
	</tr>
	<tr class="paypal-fields">
		<th scope="row"><label for="paypal_api_password"><?php echo __('API Password', THIS_PLUGIN_NAME); ?></label></th>
		<td><input type="password" name="paypal_api_password" value="<?php echo $config->paypal_api_password ?>" /></td>
	</tr>
	<tr class="paypal-fields">
		<th scope="row"><label for="paypal_api_signature"><?php echo __('API Signature', THIS_PLUGIN_NAME); ?></label></th>
		<td><input type="text" name="paypal_api_signature" value="<?php echo $config->paypal_api_signature ?>" /></td>
	</tr>

	<tr class="paypal-sandbox-fields">
		<th scope="row"><label for="paypal_email_sandbox"><?php echo __('Paypal sandbox email', THIS_PLUGIN_NAME); ?></label></th>
		<td><input type="email" name="paypal_email_sandbox" value="<?php echo $config->paypal_email_sandbox ?>" /></td>
	</tr>
	<tr class="paypal-sandbox-fields">
		<th scope="row"><label for="paypal_api_username_sandbox"><?php echo __('API Username sandbox', THIS_PLUGIN_NAME); ?></label></th>
		<td><input type="text" name="paypal_api_username_sandbox" value="<?php echo $config->paypal_api_username_sandbox ?>" /></td>
	</tr>
	<tr class="paypal-sandbox-fields">
		<th scope="row"><label for="paypal_api_password_sandbox"><?php echo __('API Password sandbox', THIS_PLUGIN_NAME); ?></label></th>
		<td><input type="password" name="paypal_api_password_sandbox" value="<?php echo $config->paypal_api_password_sandbox ?>" /></td>
	</tr>
	<tr class="paypal-sandbox-fields">
		<th scope="row"><label for="paypal_api_signature_sandbox"><?php echo __('API Signature sandbox', THIS_PLUGIN_NAME); ?></label></th>
		<td><input type="text" name="paypal_api_signature_sandbox" value="<?php echo $config->paypal_api_signature_sandbox ?>" /></td>
	</tr>

</table>
<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php echo __('Save Changes');?>"  /></p>
</form>