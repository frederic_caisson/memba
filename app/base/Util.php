<?php

class Util {

    public static function encrypt($text)
    {
      $salt = substr(AUTH_SALT, 0, 8);
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

    public static function decrypt($text)
    {
      $salt = substr(AUTH_SALT, 0, 8);
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }
    
	  public static function isValidTimezone($timezone) {
      $zoneList = timezone_identifiers_list();
      return in_array($timezone, $zoneList);
    }

    public static function randomPassword($len = 8){

      if(($len%2)!==0){ 
        $len=8;
      }
      $length=$len-2; 
      $conso=array('b','c','d','f','g','h','j','k','l','m','n','p','r','s','t','v','w','x','y','z');
      $vocal=array('a','e','i','o','u');
      $password='';
      srand ((double)microtime()*1000000);
      $max = $length/2;
      for($i=1; $i<=$max; $i++){
        $password.=$conso[rand(0,19)];
        $password.=$vocal[rand(0,4)];
      }
      $password.=rand(10,99);
      $newpass = $password;
      return $newpass;
    }

    public static function addPeriod($date, $interval, $period){
      $new_date = clone $date;
      $add = '+'.$interval;
      if($period == 'D') {
        $add = $add . ' day';
      } else if ($period == 'W'){
        $add = $add . ' week';
      } else if ($period == 'M'){
        $add = $add . ' month';
      } else if ($period== 'Y'){
        $add = $add . ' year';
      }
      return $new_date->modify("$add");
    }

    public static function getTimezones() {
      $zones_array = array();
      $timestamp = time();
      foreach(timezone_identifiers_list() as $key => $zone) {
        date_default_timezone_set($zone);
        $zones_array[$key]['zone'] = $zone;
        $zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
      }
      return $zones_array;
    }

}

?>