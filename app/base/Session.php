<?php

class Session{
	
    public static function resetSession(){
      $_SESSION[THIS_PLUGIN_NAME.'_session'] = null;
    }

    public static function createSession($id){
      $encrypted = Util::encrypt($id);
      $_SESSION[THIS_PLUGIN_NAME.'_session'] = $encrypted;
    }

    public function getSession(){
    	if (isset($_SESSION[THIS_PLUGIN_NAME.'_session'])) {
    		return $_SESSION[THIS_PLUGIN_NAME.'_session'];
    	}
    	return null;
    }

    public static function getConnectedUser(){
    	$result = false;
    	if (isset($_SESSION[THIS_PLUGIN_NAME.'_session'])) {
    		$userId = Util::decrypt($_SESSION[THIS_PLUGIN_NAME.'_session']);
    		$userModel = new User();
    		$user = $userModel->findById($userId);

    		if($user) {
    			$result = $user;
    		}		
    	}
		  return $result;
	  }
}
?>