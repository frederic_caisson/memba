<?php
class View 
{
	private $view = null;
	private $vars = null;
	private $includeViews = null;

	public function __construct($view) {
		$this->includeViews = array();
		$this->vars = array();
	    $path = THIS_PLUGIN_BASE_DIR."app/view/".$view.".view.php";
	    if (file_exists($path)) {
	        $this->view = $path;
	    } else {
	        wp_die(__("View ".$path." not found"));
	    }
	}

	public function set($name,$value) {
    	$this->vars[$name] = $value;
	}

	public function includeView($name) {
		$this->includeViews[] = $name;
	}

	public function render() {
	    extract($this->vars,EXTR_SKIP);
	    ob_start();

	    foreach($this->includeViews as $name) {
	    	 include THIS_PLUGIN_BASE_DIR."app/view/".$name.".view.php";
	    }
	    include $this->view;
	    echo ob_get_clean();
	}  
}
?>