<?php
	abstract class Model {
		
		protected $wpdb;

		const NULL = 'NULL';

		public function __construct() {
			global $wpdb;
			$this->wpdb = &$wpdb;
			add_filter( 'query', array($this,'manageNull') );
		}

		function manageNull( $query )
		{
		    return str_ireplace( "'NULL'", "NULL", $query ); 
		}
	}
?>