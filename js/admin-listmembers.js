( function( $ ) {
	$(".tablesorter").tablesorter(); 
	$('#message').empty();
	$('#message').removeClass('updated error');

	$('.submitdelete').click(function(event){
		event.preventDefault();
		$('#message').empty();
		$('#message').removeClass('updated error');
		if(confirm(ajax_object.confirm_message)){
			var id = $(this).attr('value');
			var data = {
				'action': 'delete_member',
				'id' : id
			};

			$.post(ajax_object.ajax_url, data, function(response) {
				var json = JSON.parse(response);
				if(json.result == 1){
					$( '.row'+id ).fadeOut( "slow" );
					$('#message').addClass( 'updated' );
				} else {
					$('#message').addClass( 'error' );
				}
				$('#message').append('<p>'+json.message+'</p>');

			});
		}

	});
})( jQuery );