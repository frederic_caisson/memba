( function( $ ) {
	$('#form_settings').submit(function(event){
		event.preventDefault();
		$( "#message" ).removeClass( "m-message-ok" );
		$( "#message" ).removeClass( "m-message-error" );
		$('#message').hide();
		$('#message').empty();

		var email = $('#form_settings').find('input[name="email"]').val();
		var first_name = $('#form_settings').find('input[name="first_name"]').val();
		var last_name = $('#form_settings').find('input[name="last_name"]').val();
		var postal = $('#form_settings').find('input[name="postal"]').val();
		var tel = $('#form_settings').find('input[name="tel"]').val();
		var address1 = $('#form_settings').find('input[name="address1"]').val();
		var address2 = $('#form_settings').find('input[name="address2"]').val();
		var password = $('#form_settings').find('input[name="password"]').val();
		var confirm = $('#form_settings').find('input[name="confirm"]').val();

		var data = {
		'action': 'save_member',
		'email': email,
		'password': password,
		'confirm': confirm,
		'first_name': first_name,
		'last_name': last_name,
		'postal': postal,
		'tel': tel,
		'address1': address1,
		'address2': address2,
		};
		$.post(ajax_object.ajax_url, data, function(response) {
			var json = JSON.parse(response);

			if(json.result == 1) {

				$( "#message" ).addClass( "m-message-ok" );	
				$('#message').append(json.message);
				$('#message').toggle();

			} else {

				if(json.result == 0) {
					$( "#message" ).addClass( "m-message-error" );
					$('#message').append(json.message);
					$('#message').toggle();
				}
			}
		});
	});
})( jQuery );
