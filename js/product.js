( function( $ ) {
	var t = new Date().getTimezoneOffset();
	$('.custom-field').each(function(){
    	var v = $(this).val() 
    	if(v.indexOf("timezone") == -1){
    		v = v + '&timezone=' + t;
    		$(this).val(v);
    	}
	});
})( jQuery );
