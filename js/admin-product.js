( function( $ ) {
	$('#type').change(function () {
		var type = $( this ).val();
		if(type == 1){
			$('.trial-fields').hide();
			$('.sell-period').hide();
			$('.cycle-fields').hide();
			$('#sell_price_label').text( sell_price_label ); 
		} else if (type == 2){
			$('.trial-fields').hide();
			$('.sell-period').show();
			$('.cycle-fields').hide();
			$('#sell_price_label').text( sell_price_label ); 
			$('#sell_price_label2').text( sell_price_limited_label ); 
		} else if (type == 3){
			$('.trial-fields').hide();
			$('.sell-period').show();
			$('.cycle-fields').hide();
			$('#sell_price_label').text( sell_price_label );
			$('#sell_price_label2').text( sell_price_every_label ); 
		} else if (type == 4){
			$('.trial-fields').show();
			$('.sell-period').show();
			$('.cycle-fields').hide();
			$('#sell_price_label').text( sell_price_trial_label ); 
			$('#sell_price_label2').text( sell_price_every_label ); 
		} else if (type == 5){
			$('.trial-fields').hide();
			$('.sell-period').show();
			$('.cycle-fields').show();
			$('#sell_price_label').text( sell_price_label );
			$('#sell_price_label2').text( sell_price_every_label ); 
		} else if (type == 6){
			$('.trial-fields').show();
			$('.sell-period').show();
			$('.cycle-fields').show();
			$('#sell_price_label').text( sell_price_trial_label ); 
			$('#sell_price_label2').text( sell_price_every_label ); 
		} 
	}).change();
})( jQuery );