$ = jQuery.noConflict();
$(document).ready(function() {
	var tz = jstz.determine(); // Determines the time zone of the browser client
	var tn = tz.name();
	$('#timezone').val(tn);

	$('#form_checkout').submit(function(event){
		event.preventDefault();
		$('#message').hide();
		$('#message').empty();

		email = '';
		password = '';
		paypal_email = '';

		if($('#email').length > 0){
			email = $('#email').val().trim();
		}
		if($('#password').length > 0){
			password = $('#password').val().trim();
		}
		if($('#paypal_email').length > 0){
			paypal_email = $('#paypal_email').val().trim();
		}
		timezone = $('#timezone').val().trim();
		product_id = $('#product_id').val().trim();

		var data = {
		'action': 'process',
		'email': email,
		'password': password,
		'paypal_email': paypal_email,
		'timezone' : timezone,
		'product_id' : product_id
		};

		$.post(ajax_object.ajax_url, data, function(response) {
			var json = JSON.parse(response);
			if (json.result == 0) {
				$('#message').append(json.message);
				$('#message').toggle();
			} else if(json.result == 1){
				window.location.href = json.redirect;
			}
		});
		
	});

	$( "#email" ).focus(function() {
		$( "#email" ).prop('required',true);
		$( "#password" ).prop('required',true);
		$( "#paypal_email" ).prop('required',false);
		$( "#paypal_email" ).removeAttr('value');
	});
	$( "#password" ).focus(function() {
		$( "#email" ).prop('required',true);
		$( "#password" ).prop('required',true);
		$( "#paypal_email" ).prop('required',false);
		$( "#paypal_email" ).removeAttr('value');
	});
	$( "#paypal_email" ).focus(function() {
		$( "#email" ).prop('required',false);
		$( "#password" ).prop('required',false);
		$( "#paypal_email" ).prop('required',true);
		$('#email').removeAttr('value');
		$('#password').removeAttr('value');
	});
});