( function( $ ) {
	$('#form_login').submit(function(event){
		event.preventDefault();
		$('#message').hide();
		$('#message').empty();
		var email = $('#form_login').find('input[name="email"]').val();
		var password = $('#form_login').find('input[name="password"]').val();

		var data = {
		'action': 'check_login',
		'email': email,
		'password': password,
		};
		$.post(ajax_object.ajax_url, data, function(response) {
			var json = JSON.parse(response);

			if (json.result == 0) {
				
				$('#message').append(json.message);
				$('#message').toggle();

			} else {
				window.location.href = ajax_object.redirect_url;
			}
		});
	});
})( jQuery );
