( function( $ ) {

	function radio_change(){
		if($("#mode0").is(":checked")){
			$(".paypal-fields").hide();
		    $(".paypal-sandbox-fields").show();
		} else {
			$(".paypal-fields").show();
		    $(".paypal-sandbox-fields").hide();
		}
	}
	radio_change();

	$("input[name='mode']").change(function(){
		radio_change();
	});


})( jQuery );