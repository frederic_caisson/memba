function isNumberDash(event){
    var charCode = (event.which) ? event.which : event.keyCode
    if (charCode != 45 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function isNumber(event){
    var charCode = (event.which) ? event.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function isFloat(value, event) {
	 var charInt = (event.which) ? event.which : event.keyCode
	 var charString = String.fromCharCode(charInt);

	 if(charInt != 8){
		 var number = parseFloat(value + charString);
		 if (number != value + charString){
		     return false;
		 }else{
		     return true;
		 }
	 } else {

	 	return true; 
	 }
}
