( function( $ ) {

	$('#message').empty();
	$('#message').removeClass('error');
    $("input[name$='page_name_login_page']").val('');
    $("input[name$='page_name_dasboard_page']").val('');

	$('.submitdelete').click(function(event){
		event.preventDefault();
		$('#message').empty();
		$('#message').removeClass('error');
		if(confirm(ajax_object.confirm_message)){
			var page_id = $(this).attr('data-id');
			
			var data = {
				'action': 'delete_page',
				'page_id' : page_id
			};

			$.post(ajax_object.ajax_url, data, function(response) {

				var json = JSON.parse(response);
				if(json.result == 1){
					location.reload();
				} else {
					$('#message').addClass( 'error' );
					$('#message').append('<p>'+json.message+'</p>');
				}
			});
		}

	});

	$( "form[name^='form_create']" ).submit(function(event){
		event.preventDefault();
		var form_id = $(this).attr('id');
		var slug = $(this).attr('data-slug');

		var input_name = 'page_name_' + slug;
		var input_name = "#"+form_id+ " input[name='"+input_name+"']";
		var page_name = $(input_name).val();

		var data = {
				'action': 'create_page',
				'slug' :  slug,
				'page_name': page_name
			};
		$.post(ajax_object.ajax_url, data, function(response) {
			var json = JSON.parse(response);
			if(json.result == 1){
				location.reload();
			}
		});
	});	

})( jQuery );